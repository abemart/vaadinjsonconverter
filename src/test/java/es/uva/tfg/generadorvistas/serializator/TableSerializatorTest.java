package es.uva.tfg.generadorvistas.serializator;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.fge.jackson.JsonLoader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.vaadin.ui.Table;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeTable;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;
import es.uva.tfg.generadorvistas.validator.JSONValidator;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;
import junit.framework.Assert;

public class TableSerializatorTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(TableSerializatorTest.class);

	private static final String COLUMN_HEADER = "Nombre usuario";
	private static final String COLUMN_PROPERTY = "usuario";

	private static TableSerializator serializator;
	private static JSONValidator jsonSchema;
	private static Gson gson;

	@BeforeClass
	public static void init() {
		serializator = new TableSerializator();
		gson = new GsonBuilder().setPrettyPrinting().create();
		jsonSchema = JSONValidator.getInstance();
	}

	@Test
	public void convertTable() throws ComponentNotSupportedException {
		LOG.info("INICIO del test de serialización de Table");
		Table table = new Table();
		table.addContainerProperty(COLUMN_PROPERTY, String.class, "");

		table.setColumnHeader(COLUMN_PROPERTY, COLUMN_HEADER);

		long timeInit = System.currentTimeMillis();
		JsonNodeTable jsonTable = serializator.serialize(table);
		long timeFin = System.currentTimeMillis();
		double time = ((double) timeFin - timeInit) / 1000d;

		JsonObject json = jsonTable.getJsonObject();
		JsonObject jsTable = json.get("Table").getAsJsonObject();
		JsonArray jsColumns = jsTable.get("columns").getAsJsonArray();

		int size = jsColumns.size();
		Assert.assertEquals(size, 1);

		JsonObject jsColumn1 = jsColumns.get(0).getAsJsonObject();
		String header = jsColumn1.get("header").getAsString();
		String property = jsColumn1.get("propertyId").getAsString();

		Assert.assertEquals(header, COLUMN_HEADER);
		Assert.assertEquals(property, COLUMN_PROPERTY);

		String jsonStr = gson.toJson(json);

		boolean schemaOk = false;
		try {
			schemaOk = jsonSchema.validate(JsonLoader.fromString(jsonStr));
		} catch (JSONValidationException | IOException e) {
			LOG.error("Validación del esquema no válida");
		}

		Assert.assertTrue(schemaOk);

		LOG.info("\tTiempo empleado en la serialización: " + time);
		LOG.info("\tResultado obtenido: \n" + jsonStr);
		LOG.info("FIN del test de serialización de Table");
	}

}
