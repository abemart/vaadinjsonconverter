package es.uva.tfg.generadorvistas.serializator;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.fge.jackson.JsonLoader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.ui.Button;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeAbstractComponent;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;
import es.uva.tfg.generadorvistas.validator.JSONValidator;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;
import junit.framework.Assert;

public class AbsctractComponentSerializatorTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(AbsctractComponentSerializatorTest.class);

	private static final String CAPTION = "Aceptar";
	private static final Resource ICON = FontAwesome.DESKTOP;

	private static AbstractComponentSerializator serializator;
	private static JSONValidator jsonSchema;
	private static Gson gson;

	@BeforeClass
	public static void init() {
		serializator = new AbstractComponentSerializator();
		gson = new GsonBuilder().setPrettyPrinting().create();
		jsonSchema = JSONValidator.getInstance();
	}

	@Test
	public void convertButtonToJson() throws ComponentNotSupportedException {
		LOG.info("INICIO del test de serialización de un AbstractComponent");
		Button btnAceptar = new Button(CAPTION);
		btnAceptar.setIcon(ICON);

		long timeInit = System.currentTimeMillis();
		JsonNodeAbstractComponent jsonButton = serializator
				.serialize(btnAceptar);
		long timeFin = System.currentTimeMillis();
		double time = ((double) timeFin - timeInit) / 1000d;

		JsonObject json = jsonButton.getJsonObject();
		JsonObject jsButton = json.get("Button").getAsJsonObject();
		String caption = jsButton.get("caption").getAsString();
		String iconPath = jsButton.get("iconPath").getAsString();

		Assert.assertEquals(caption, CAPTION);
		Assert.assertEquals(iconPath, ICON.toString());

		String jsonStr = gson.toJson(json);

		boolean validateOk = false;

		try {
			validateOk = jsonSchema.validate(JsonLoader.fromString(jsonStr));
		} catch (JSONValidationException | IOException e) {
			LOG.error("Validación del esquema no válida");
		}

		Assert.assertTrue(validateOk);

		LOG.info("\tTiempo empleado en la serialización: " + time);
		LOG.info("\tButton obtenido: \n" + jsonStr);
		LOG.info("FIN del test de serialización de un AbstractComponent");
	}

}
