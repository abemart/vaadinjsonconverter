package es.uva.tfg.generadorvistas.serializator;

import java.io.IOException;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.fge.jackson.JsonLoader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeComponent;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeTab;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeTabSheet;
import es.uva.tfg.generadorvistas.validator.JSONValidator;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;

import static junit.framework.Assert.*;

public class TabSheetSerializatorTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(TabSheetSerializatorTest.class);

	private static final String TAB_1_CAPTION = "Tab 1";
	private static final String TAB_2_CAPTION = "Tab 2";

	private static TabSheetSerializator serializator;
	private static JSONValidator jsonSchema;
	private static Gson gson;

	@BeforeClass
	public static void init() {
		serializator = new TabSheetSerializator();
		gson = new GsonBuilder().setPrettyPrinting().create();
		jsonSchema = JSONValidator.getInstance();
	}

	@Test
	public void convertTabSheet() {
		LOG.info("INICIO del test de serialización de TabSheet");
		TabSheet tabSheet = new TabSheet();

		TextField tf1 = new TextField();
		TextField tf2 = new TextField();

		tabSheet.addTab(tf1, TAB_1_CAPTION, null);
		tabSheet.addTab(tf2, TAB_2_CAPTION, FontAwesome.AMAZON);

		long timeInit = System.currentTimeMillis();
		JsonNodeTabSheet jsonTabSheet = serializator.serialize(tabSheet);
		long timeFin = System.currentTimeMillis();
		double time = ((double) timeFin - timeInit) / 1000d;

		List<JsonNodeTab> tabs = jsonTabSheet.getTabs();
		JsonNodeTab tab1 = tabs.get(0);
		JsonNodeTab tab2 = tabs.get(1);

		// Comprobamos el número de Tabs
		assertEquals(tabs.size(), 2);

		// Comprobamos los titulos de las Tabs
		assertEquals(tab1.getCaption(), TAB_1_CAPTION);
		assertEquals(tab2.getCaption(), TAB_2_CAPTION);

		// Comprobamos los iconos de las tabs
		assertEquals(tab1.getIcon(), null);
		assertEquals(tab2.getIcon(), FontAwesome.AMAZON.toString());

		// Comprobamos los contenidos de las tabs
		JsonNodeComponent component1 = tab1.getContent();
		JsonNodeComponent component2 = tab2.getContent();
		assertEquals(component1.getComponentClass(), TextField.class);
		assertEquals(component2.getComponentClass(), TextField.class);

		JsonObject json = jsonTabSheet.getJsonObject();
		String jsonStr = gson.toJson(json);

		boolean schemaOk = false;

		try {
			schemaOk = jsonSchema.validate(JsonLoader.fromString(jsonStr));
		} catch (JSONValidationException | IOException e) {
			LOG.error("Validación del esquema no válida");
		}

		assertTrue(schemaOk);

		LOG.info("\tTiempo empleado en la serialización: " + time);
		LOG.info("\tResultado obtenido: \n" + jsonStr);
		LOG.info("FIN del test de serialización de TabSheet");
	}

}
