package es.uva.tfg.generadorvistas.serializator;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.fge.jackson.JsonLoader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeAbstractOrderedLayout;
import es.uva.tfg.generadorvistas.validator.JSONValidator;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;
import junit.framework.Assert;

public class AbstractOrderedSerializatorTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractOrderedSerializatorTest.class);

	private static final String CAPTION_BUTTON = "Aceptar";

	private static AbstractOrderedLayoutSerializator serializator;
	private static JSONValidator jsonSchema;
	private static Gson gson;

	@BeforeClass
	public static void init() {
		serializator = new AbstractOrderedLayoutSerializator();
		gson = new GsonBuilder().setPrettyPrinting().create();
		jsonSchema = JSONValidator.getInstance();
	}

	@Test
	public void serializeHorizontalLayout() {
		LOG.info("INICIO del test de serialización de AbstractOrderedLayout");
		HorizontalLayout hlPrincipal = new HorizontalLayout();
		Button btnAceptar = new Button(CAPTION_BUTTON);

		hlPrincipal.addComponent(btnAceptar);

		long timeInit = System.currentTimeMillis();
		JsonNodeAbstractOrderedLayout jsonNode = serializator
				.serialize(hlPrincipal);
		long timeFin = System.currentTimeMillis();
		double time = ((double) timeFin - timeInit) / 1000d;

		JsonObject json = jsonNode.getJsonObject();
		JsonObject jsHorizontal = json.get("HorizontalLayout")
				.getAsJsonObject();
		JsonArray jsComponents = jsHorizontal.get("components")
				.getAsJsonArray();

		int sizeComponents = jsComponents.size();
		Assert.assertEquals(sizeComponents, 1);

		JsonObject jsComponent1 = jsComponents.get(0).getAsJsonObject();
		JsonObject jsButton = jsComponent1.get("Button").getAsJsonObject();

		String caption = jsButton.get("caption").getAsString();
		Assert.assertEquals(caption, CAPTION_BUTTON);

		String jsonStr = gson.toJson(json);

		boolean schemaOk = false;

		try {
			schemaOk = jsonSchema.validate(JsonLoader.fromString(jsonStr));
		} catch (JSONValidationException | IOException e) {
			LOG.error("Validación del esquema no válida");
		}

		Assert.assertTrue(schemaOk);

		LOG.info("\tTiempo empleado en la serialización: " + time);
		LOG.info("\tResultado obtenido: \n" + jsonStr);
		LOG.info("FIN del test de serialización de AbstractOrderedLayout");
	}

}
