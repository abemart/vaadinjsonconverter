package es.uva.tfg.generadorvistas.serializator;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.fge.jackson.JsonLoader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.vaadin.ui.MenuBar;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeMenuBar;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;
import es.uva.tfg.generadorvistas.validator.JSONValidator;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;
import junit.framework.Assert;

public class MenuBarSerializatorTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(MenuBarSerializatorTest.class);

	private static final String MENU_ITEM_1_CAPTION = "Admin";
	private static final String MENU_ITEM_2_CAPTION = "Users";

	private static MenuBarSerializator serializator;
	private static JSONValidator jsonSchema;
	private static Gson gson;

	@BeforeClass
	public static void init() {
		serializator = new MenuBarSerializator();
		gson = new GsonBuilder().setPrettyPrinting().create();
		jsonSchema = JSONValidator.getInstance();
	}

	@Test
	public void convertMenuBar() throws ComponentNotSupportedException {
		LOG.info("INICIO del test de serialización de MenuBar");
		MenuBar menuBar = new MenuBar();
		menuBar.addItem(MENU_ITEM_1_CAPTION, null);
		menuBar.addItem(MENU_ITEM_2_CAPTION, null);

		long timeInit = System.currentTimeMillis();
		JsonNodeMenuBar jsonMenuBar = serializator.serialize(menuBar);
		long timeFin = System.currentTimeMillis();
		double time = ((double) timeFin - timeInit) / 1000d;

		JsonObject json = jsonMenuBar.getJsonObject();
		JsonObject jsMenuBar = json.get("MenuBar").getAsJsonObject();
		JsonArray jsMenuItems = jsMenuBar.get("menuItems").getAsJsonArray();

		int size = jsMenuItems.size();
		Assert.assertEquals(size, 2);

		JsonObject jsMenuItem1 = jsMenuItems.get(0).getAsJsonObject();
		String text1 = jsMenuItem1.get("text").getAsString();
		JsonObject jsMenuItem2 = jsMenuItems.get(1).getAsJsonObject();
		String text2 = jsMenuItem2.get("text").getAsString();

		Assert.assertEquals(text1, MENU_ITEM_1_CAPTION);
		Assert.assertEquals(text2, MENU_ITEM_2_CAPTION);

		String jsonStr = gson.toJson(json);

		boolean schemaOk = false;
		try {
			schemaOk = jsonSchema.validate(JsonLoader.fromString(jsonStr));
		} catch (JSONValidationException | IOException e) {
			LOG.error("Validación del esquema no válida");
		}

		Assert.assertTrue(schemaOk);

		LOG.info("\tTiempo empleado en la serialización: " + time);
		LOG.info("\tResultado obtenido: \n" + jsonStr);
		LOG.info("FIN del test de serialización de MenuBar");
	}

}
