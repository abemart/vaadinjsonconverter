package es.uva.tfg.generadorvistas.parser;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.TextField;

import es.uva.tfg.generadorvistas.components.JSONAbstractComponent;
import es.uva.tfg.generadorvistas.translater.JSONTranslater;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;
import junit.framework.Assert;

public class AbstractComponentParserTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractComponentParserTest.class);

	private static final String TEXT_FIELD_ID = "tfNombre";
	private static final String TEXT_FIElD_CAPTION = "Nombre de usuario";
	private static final boolean TEXT_FIELD_VISIBLE = true;
	private static final float TEXT_FIELD_WIDTH_MEASURE = 100;
	private static final String TEXT_FIELD_WIDTH_UNIT = "px";
	private static final float TEXT_FIELD_HEIGHT_MEASURE = 25;
	private static final String TEXT_FIELD_HEIGHT_UNIT = "px";

	private static JsonNode abstractFieldJson;

	@BeforeClass
	public static void initJson() throws IOException {
		abstractFieldJson = JsonLoader
				.fromResource("/parser/examples/TextField.json");
	}

	@Test
	public void parserAbstractFieldComponent() throws JSONValidationException {
		LOG.info("Inicio del test de parseo de un Abstract Field");
		JSONTranslater translater = new JSONTranslater();
		translater.setJson(abstractFieldJson);
		
		long timeInicio = System.currentTimeMillis();
		JSONAbstractComponent jsonComponent = (JSONAbstractComponent) translater
				.translate();
		long timeFin = System.currentTimeMillis();

		double time = ((double) timeFin - timeInicio) / 1000d;
		LOG.info("\tTiempo empleado en la traducción: " + time);

		TextField textField = (TextField) jsonComponent.getVaadinComponent();
		TextField textFieldId = (TextField) jsonComponent
				.getVaadinComponent(TEXT_FIELD_ID);

		// Comprobamos que tienen que ser iguales
		Assert.assertEquals(textField, textFieldId);

		// Comprobamos las propiedades
		String id = textField.getId();
		String caption = textField.getCaption();
		boolean visible = textField.isVisible();
		float width = textField.getWidth();
		float height = textField.getHeight();
		Unit widthUnit = textField.getWidthUnits();
		Unit heightUnit = textField.getHeightUnits();

		Assert.assertEquals(id, TEXT_FIELD_ID);
		Assert.assertEquals(caption, TEXT_FIElD_CAPTION);
		Assert.assertEquals(visible, TEXT_FIELD_VISIBLE);
		Assert.assertEquals(width, TEXT_FIELD_WIDTH_MEASURE);
		Assert.assertEquals(height, TEXT_FIELD_HEIGHT_MEASURE);
		Assert.assertEquals(widthUnit.getSymbol(), TEXT_FIELD_WIDTH_UNIT);
		Assert.assertEquals(heightUnit.getSymbol(), TEXT_FIELD_HEIGHT_UNIT);

		LOG.info("Fin del test de parseo de un Abstract Field");
	}

}
