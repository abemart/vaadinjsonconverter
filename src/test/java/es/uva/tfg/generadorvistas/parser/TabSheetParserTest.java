package es.uva.tfg.generadorvistas.parser;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.vaadin.ui.TabSheet;

import es.uva.tfg.generadorvistas.components.JSONAbstractComponent;
import es.uva.tfg.generadorvistas.components.JSONTabSheetComponent;
import es.uva.tfg.generadorvistas.translater.JSONTranslater;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;
import junit.framework.Assert;

public class TabSheetParserTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(MenuBarParserTest.class);

	private static final String ID = "tabPrincipal";
	private static final float WIDTH = 100;
	private static final String WIDTH_UNIT = "%";
	private static final float HEIGHT = 100;
	private static final String HEIGHT_UNIT = "%";

	private static final String ID_BUTTON = "btnAceptar";

	private static JsonNode tabSheetJson;

	@BeforeClass
	public static void initJson() throws IOException {
		tabSheetJson = JsonLoader
				.fromResource("/parser/examples/TabSheet.json");
	}

	@Test
	public void parseTabSheet() throws JSONValidationException {
		LOG.info("Inicio del Test del TabSheet Parser");
		JSONTranslater translater = new JSONTranslater();
		translater.setJson(tabSheetJson);

		long timeInicio = System.currentTimeMillis();
		JSONTabSheetComponent jsonTabSheet = (JSONTabSheetComponent) translater
				.translate();
		long timeFin = System.currentTimeMillis();

		double time = ((double) timeFin - timeInicio) / 1000d;
		LOG.info("\tTiempo empleado en el parseo: " + time);

		TabSheet tabSheet = jsonTabSheet.getVaadinComponent();
		TabSheet tabSheetId = (TabSheet) jsonTabSheet.getVaadinComponent(ID);

		// Comprobamos que son iguales al obtener el menuBar por el ID
		Assert.assertEquals(tabSheet, tabSheetId);

		String id = tabSheet.getId();
		float width = tabSheet.getWidth();
		String unitWidth = tabSheet.getWidthUnits().getSymbol();
		float height = tabSheet.getHeight();
		String unitHeight = tabSheet.getHeightUnits().getSymbol();

		JSONAbstractComponent componentButton = (JSONAbstractComponent) jsonTabSheet
				.getJSONComponent(ID_BUTTON);

		// Comprobamos que no es nulo el boton que existe.
		Assert.assertNotNull(componentButton);

		Assert.assertEquals(id, ID);
		Assert.assertEquals(width, WIDTH);
		Assert.assertEquals(unitWidth, WIDTH_UNIT);
		Assert.assertEquals(height, HEIGHT);
		Assert.assertEquals(unitHeight, HEIGHT_UNIT);

		LOG.info("Fin del Test del TabSheet Parser");
	}
}
