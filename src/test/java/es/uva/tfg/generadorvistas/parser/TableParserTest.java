package es.uva.tfg.generadorvistas.parser;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.vaadin.ui.Table;

import es.uva.tfg.generadorvistas.components.JSONTableComponent;
import es.uva.tfg.generadorvistas.translater.JSONTranslater;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;
import junit.framework.Assert;

public class TableParserTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(MenuBarParserTest.class);

	private static final String ID = "tblUsuarios";
	private static final float WIDTH = 100;
	private static final String WIDTH_UNIT = "%";
	private static final float HEIGHT = 100;
	private static final String HEIGHT_UNIT = "%";

	private static JsonNode tableJson;

	@BeforeClass
	public static void initJson() throws IOException {
		tableJson = JsonLoader.fromResource("/parser/examples/Table.json");
	}

	@Test
	public void parseTable() throws JSONValidationException {
		LOG.info("Inicio del Test del Table Parser");
		JSONTranslater translater = new JSONTranslater();
		translater.setJson(tableJson);

		long timeInicio = System.currentTimeMillis();
		JSONTableComponent jsonTable = (JSONTableComponent) translater
				.translate();
		long timeFin = System.currentTimeMillis();

		double time = ((double) timeFin - timeInicio) / 1000d;
		LOG.info("\tTiempo empleado en el parseo: " + time);

		Table table = jsonTable.getVaadinComponent();
		Table tableId = (Table) jsonTable.getVaadinComponent(ID);

		// Comprobamos que son iguales al obtener el menuBar por el ID
		Assert.assertEquals(table, tableId);

		String id = table.getId();
		float width = table.getWidth();
		String unitWidth = table.getWidthUnits().getSymbol();
		float height = table.getHeight();
		String unitHeight = table.getHeightUnits().getSymbol();

		Assert.assertEquals(id, ID);
		Assert.assertEquals(width, WIDTH);
		Assert.assertEquals(unitWidth, WIDTH_UNIT);
		Assert.assertEquals(height, HEIGHT);
		Assert.assertEquals(unitHeight, HEIGHT_UNIT);

		LOG.info("Fin del Test del Table Parser");
	}
}
