package es.uva.tfg.generadorvistas.parser;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;

import es.uva.tfg.generadorvistas.components.JSONAbstractOrderedLayout;
import es.uva.tfg.generadorvistas.translater.JSONTranslater;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;
import junit.framework.Assert;

public class AbstractOrderedLayoutParserTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractOrderedLayoutParserTest.class);

	private static final String ID = "hlAcciones";
	private static final float WIDHT = 100;
	private static final String WIDTH_UNIT = "%";
	private static final String TEXTFIELD_ID = "tfNombre";
	private static final String TEXTFIELD_CAPTION = "Nombre";
	private static final String BTN_ACEPTAR_ID = "btnAceptar";
	private static final String BTN_ACEPTAR_CAPTION = "Aceptar";

	private static JsonNode horizontalLayoutJson;

	@BeforeClass
	public static void initJson() throws IOException {
		horizontalLayoutJson = JsonLoader
				.fromResource("/parser/examples/HorizontalLayout.json");
	}

	@Test
	public void parserAbstractOrderedLayoutComponent()
			throws JSONValidationException {
		LOG.info("Inicio del Test del Abstract Ordered Layout Parser");
		JSONTranslater translater = new JSONTranslater();
		translater.setJson(horizontalLayoutJson);

		long timeInicio = System.currentTimeMillis();
		JSONAbstractOrderedLayout jsonComponent = (JSONAbstractOrderedLayout) translater
				.translate();
		long timeFin = System.currentTimeMillis();

		double time = ((double) timeFin - timeInicio) / 1000d;
		LOG.info("\tTiempo empleado en el parseo: " + time);

		HorizontalLayout horizontalLayout = (HorizontalLayout) jsonComponent
				.getVaadinComponent();
		HorizontalLayout horizontalLayoutID = (HorizontalLayout) jsonComponent
				.getVaadinComponent("hlAcciones");

		TextField tfNombre = (TextField) jsonComponent
				.getVaadinComponent(TEXTFIELD_ID);

		Button btnAceptar = (Button) jsonComponent
				.getVaadinComponent(BTN_ACEPTAR_ID);

		// Comprobamos que tienen que ser igual al obtenerle por ID que sin ID.
		Assert.assertEquals(horizontalLayout, horizontalLayoutID);

		// Comprobamos las propiedades
		String id = horizontalLayout.getId();
		float width = horizontalLayout.getWidth();
		Unit widthUnit = horizontalLayout.getWidthUnits();
		String idTextField = tfNombre.getId();
		String captionTextField = tfNombre.getCaption();
		String idBtnAceptar = btnAceptar.getId();
		String captionBtnAceptar = btnAceptar.getCaption();

		Assert.assertEquals(id, ID);
		Assert.assertEquals(width, WIDHT);
		Assert.assertEquals(widthUnit.getSymbol(), WIDTH_UNIT);
		Assert.assertEquals(idTextField, TEXTFIELD_ID);
		Assert.assertEquals(captionTextField, TEXTFIELD_CAPTION);
		Assert.assertEquals(idBtnAceptar, BTN_ACEPTAR_ID);
		Assert.assertEquals(captionBtnAceptar, BTN_ACEPTAR_CAPTION);

		LOG.info("Fin del Test del Abstract Ordered Layout Parser");
	}

}
