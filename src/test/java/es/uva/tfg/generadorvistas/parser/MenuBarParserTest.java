package es.uva.tfg.generadorvistas.parser;

import java.io.IOException;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

import es.uva.tfg.generadorvistas.components.JSONMenuBarComponent;
import es.uva.tfg.generadorvistas.translater.JSONTranslater;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;
import junit.framework.Assert;

public class MenuBarParserTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(MenuBarParserTest.class);

	private static final String ID = "menuBarPrincipal";
	private static final float WIDTH = 100;
	private static final String WIDTH_UNIT = "%";
	private static final int SIZE_ITEMS = 2;

	private static JsonNode menuBarJson;

	@BeforeClass
	public static void initJson() throws IOException {
		menuBarJson = JsonLoader.fromResource("/parser/examples/MenuBar.json");
	}

	@Test
	public void parseMenuBar() throws JSONValidationException {
		LOG.info("Inicio del Test del MenuBar Parser");
		JSONTranslater translater = new JSONTranslater();
		translater.setJson(menuBarJson);

		long timeInicio = System.currentTimeMillis();
		JSONMenuBarComponent jsonMenuBar = (JSONMenuBarComponent) translater
				.translate();
		long timeFin = System.currentTimeMillis();

		double time = ((double) timeFin - timeInicio) / 1000d;
		LOG.info("\tTiempo empleado en el parseo: " + time);

		MenuBar menuBar = jsonMenuBar.getVaadinComponent();
		MenuBar menuBarId = (MenuBar) jsonMenuBar.getVaadinComponent(ID);

		// Comprobamos que son iguales al obtener el menuBar por el ID
		Assert.assertEquals(menuBar, menuBarId);

		// Comprobamos que hay dos MenuItems
		List<MenuItem> menuItems = menuBar.getItems();
		Assert.assertEquals(menuItems.size(), SIZE_ITEMS);

		String id = menuBar.getId();
		float width = menuBar.getWidth();
		String unit = menuBar.getWidthUnits().getSymbol();

		Assert.assertEquals(id, ID);
		Assert.assertEquals(width, WIDTH);
		Assert.assertEquals(unit, WIDTH_UNIT);

		LOG.info("Fin del Test del MenuBar Parser");
	}

}
