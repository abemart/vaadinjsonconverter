package es.uva.tfg.generadorvistas.validator;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import es.uva.tfg.generadorvistas.validator.JSONValidator;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;

public class SchemaValidatorTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(SchemaValidatorTest.class);

	private static JSONValidator jsonSchema;

	@BeforeClass
	public static void initJsonSchemas() throws ProcessingException {
		jsonSchema = JSONValidator.getInstance();

		LOG.info("INICIALIZADO EL JSON VALIDATOR\n\n");
	}

	@Test
	public void abstractFieldSchemaTestOK()
			throws ProcessingException, IOException {
		LOG.info("------ INICIO test Abstract Field Schema OK ------");

		try {
			// JSON a validar
			JsonNode verticalLayout = JsonLoader
					.fromResource("/validator/examples/AbstractFieldOK.json");

			long start = System.currentTimeMillis();
			boolean ok = jsonSchema.validate(verticalLayout);
			long finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: true");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			// Comprobamos que ha ido bien
			assertTrue(ok);
			LOG.info("------ FIN test Abstract Field Schema OK ------\n\n");
		} catch (JSONValidationException e) {
			LOG.error("Error no esperado: " + e.getMessage());
			assertTrue(false);
		}
	}

	@Test
	public void abstractFieldSchemaTestKO()
			throws ProcessingException, IOException {
		LOG.info("------ INICIO test Abstract Field Schema KO ------");

		long start = 0, finish = 0;
		boolean ok = false;

		try {
			// JSON a validar
			JsonNode verticalLayout = JsonLoader
					.fromResource("/validator/examples/AbstractFieldKO.json");

			start = System.currentTimeMillis();
			ok = jsonSchema.validate(verticalLayout);
		} catch (JSONValidationException e) {
			finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: false");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			LOG.info("Error:" + e.getMessage());

			// Comprobamos que no ha ido bien
			assertTrue(!ok);
			LOG.info("------ FIN test Abstract Field Schema KO ------\n\n");
		}
	}

	@Test
	public void abstractOrderedLayoutSchemaTestOK()
			throws ProcessingException, IOException {
		LOG.info("------ INICIO test Abstract Ordered Layout Schema OK ------");

		try {
			// JSON a validar
			JsonNode verticalLayout = JsonLoader.fromResource(
					"/validator/examples/AbstractOrderedLayoutOK.json");

			long start = System.currentTimeMillis();
			boolean ok = jsonSchema.validate(verticalLayout);
			long finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: true");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			// Comprobamos que ha ido bien
			assertTrue(ok);
			LOG.info(
					"------ FIN test Abstract Ordered Layout Schema OK ------\n\n");
		} catch (JSONValidationException e) {
			LOG.error("Error no esperado: " + e.getMessage());
			assertTrue(false);
		}
	}

	@Test
	public void abstractOrderedLayoutSchemaTestKO()
			throws ProcessingException, IOException {
		LOG.info("------ INICIO test Abstract Ordered Layout Schema KO ------");

		long start = 0, finish = 0;
		boolean ok = false;

		try {
			// JSON a validar
			JsonNode verticalLayout = JsonLoader.fromResource(
					"/validator/examples/AbstractOrderedLayoutKO.json");

			start = System.currentTimeMillis();
			ok = jsonSchema.validate(verticalLayout);
		} catch (JSONValidationException e) {
			finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: false");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			LOG.info("Error:" + e.getMessage());

			// Comprobamos que no ha ido bien
			assertTrue(!ok);
			LOG.info(
					"------ FIN test Abstract Ordered Layout Schema KO ------\n\n");
		}
	}

	@Test
	public void menuBarSchemaTestOK() throws ProcessingException, IOException {
		LOG.info("------ INICIO test Menu Bar Schema OK ------");

		try {
			// JSON a validar
			JsonNode menuBarJson = JsonLoader
					.fromResource("/validator/examples/MenuBarOK.json");

			long start = System.currentTimeMillis();
			boolean ok = jsonSchema.validate(menuBarJson);
			long finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: true");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			// Comprobamos que ha ido bien
			assertTrue(ok);
			LOG.info("------ FIN test test Menu Bar Schema OK ------\n\n");
		} catch (JSONValidationException e) {
			LOG.error("Error no esperado: " + e.getMessage());
			assertTrue(false);
		}
	}

	@Test
	public void menuBarSchemaTestKO() throws ProcessingException, IOException {
		LOG.info("------ INICIO test Menu Bar Schema KO ------");

		long start = 0, finish = 0;
		boolean ok = false;

		try {
			// JSON a validar
			JsonNode menuBarJson = JsonLoader
					.fromResource("/validator/examples/MenuBarKO.json");

			start = System.currentTimeMillis();
			ok = jsonSchema.validate(menuBarJson);
		} catch (JSONValidationException e) {
			finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: false");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			LOG.info("Error:" + e.getMessage());

			// Comprobamos que ha ido bien
			assertTrue(!ok);
			LOG.info("------ FIN test test Menu Bar Schema KO ------\n\n");
		}
	}

	@Test
	public void tableSchemaTestOK() throws ProcessingException, IOException {
		LOG.info("------ INICIO test Table Schema OK ------");

		try {
			// JSON a validar
			JsonNode tableJson = JsonLoader
					.fromResource("/validator/examples/TableOK.json");

			long start = System.currentTimeMillis();
			boolean ok = jsonSchema.validate(tableJson);
			long finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: true");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			// Comprobamos que ha ido bien
			assertTrue(ok);
			LOG.info("------ FIN test test Table Schema OK ------\n\n");
		} catch (JSONValidationException e) {
			LOG.error("Error no esperado: " + e.getMessage());
			assertTrue(false);
		}
	}

	@Test
	public void tableSchemaTestKO() throws ProcessingException, IOException {
		LOG.info("------ INICIO test Table Schema KO ------");

		long start = 0, finish = 0;
		boolean ok = false;

		try {
			// JSON a validar
			JsonNode menuBarJson = JsonLoader
					.fromResource("/validator/examples/TableKO.json");

			start = System.currentTimeMillis();
			ok = jsonSchema.validate(menuBarJson);
		} catch (JSONValidationException e) {
			finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: false");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			LOG.info("Error:" + e.getMessage());

			// Comprobamos que ha ido bien
			assertTrue(!ok);
			LOG.info("------ FIN test test Table Schema KO ------\n\n");
		}
	}

	@Test
	public void tabSheetSchemaTestOK() throws ProcessingException, IOException {
		LOG.info("------ INICIO test TabSheet Schema OK ------");

		try {
			// JSON a validar
			JsonNode tabSheetJson = JsonLoader
					.fromResource("/validator/examples/TabSheetOK.json");

			long start = System.currentTimeMillis();
			boolean ok = jsonSchema.validate(tabSheetJson);
			long finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: true");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			// Comprobamos que ha ido bien
			assertTrue(ok);
			LOG.info("------ FIN test test TabSheet Schema OK ------\n\n");
		} catch (JSONValidationException e) {
			LOG.error("Error no esperado: " + e.getMessage());
			assertTrue(false);
		}
	}

	@Test
	public void tabSheetSchemaTestKO() throws ProcessingException, IOException {
		LOG.info("------ INICIO test TabSheet Schema KO ------");

		long start = 0, finish = 0;
		boolean ok = false;

		try {
			// JSON a validar
			JsonNode tabSheetJson = JsonLoader
					.fromResource("/validator/examples/TabSheetKO.json");

			start = System.currentTimeMillis();
			ok = jsonSchema.validate(tabSheetJson);
		} catch (JSONValidationException e) {
			finish = System.currentTimeMillis();

			double time = (finish - start) / 1000.0;

			LOG.info("Validación con éxito: " + ok + ". Esperado: false");
			LOG.info("Tiempo necesitado para la validación: " + time + " s");

			LOG.info("Error:" + e.getMessage());

			// Comprobamos que ha ido bien
			assertTrue(!ok);
			LOG.info("------ FIN test test TabSheet Schema KO ------\n\n");
		}
	}

}
