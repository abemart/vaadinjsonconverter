package es.uva.tfg.generadorvistas.translater;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class ComponentTranslaterTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(ComponentTranslaterTest.class);

	private static ComponentTranslater translater;

	@BeforeClass
	public static void init() {
		translater = new ComponentTranslater();
	}

	@Test
	public void translateAbstractOrderedLayout() {
		HorizontalLayout hlPrincipal = new HorizontalLayout();
		Button btnAceptar = new Button("Aceptar");
		btnAceptar.setId("btnAceptar");

		VerticalLayout vlSegundo = new VerticalLayout();
		vlSegundo.setId("vlSegundo");

		TextField tfNombre = new TextField("Nombre");

		vlSegundo.addComponent(tfNombre);

		hlPrincipal.addComponent(vlSegundo);
		hlPrincipal.addComponent(btnAceptar);
		hlPrincipal.setComponentAlignment(btnAceptar, Alignment.MIDDLE_CENTER);
		hlPrincipal.setExpandRatio(vlSegundo, 1.0f);

		String json = translater.translate(hlPrincipal);
		LOG.info("HorizontalLayout: \n" + json);
	}

	@Test
	public void translateMenuBar() {
		MenuBar menuBar = new MenuBar();
		menuBar.setId("menuBar");

		MenuItem admin = menuBar.addItem("Administración", null);
		admin.addItem("Usuarios", null);
		menuBar.addItem("Mis tareas", null);

		String json = translater.translate(menuBar);
		LOG.info("MenuBar: \n" + json);
	}

	@Test
	public void translateTable() {
		Table table = new Table();
		table.addContainerProperty("username", String.class, "");
		table.addContainerProperty("name", String.class, "");

		table.setColumnHeader("username", "Nombre usuario");
		table.setColumnHeader("name", "Nombre");

		String jsonTable = translater.translate(table);
		LOG.info("Table: \n" + jsonTable);
	}

	@Test
	public void translateTabSheet() {
		TabSheet tabSheet = new TabSheet();

		TextField tf1 = new TextField();
		TextField tf2 = new TextField();

		tabSheet.addTab(tf1, "Tab 1", null);
		tabSheet.addTab(tf2, "Tab 2", FontAwesome.AMAZON);

		String jsonTabSheet = translater.translate(tabSheet);
		LOG.info("TabSheet: \n" + jsonTabSheet);
	}

}
