package es.uva.tfg.generadorvistas.translater;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.vaadin.ui.HorizontalLayout;

import es.uva.tfg.generadorvistas.components.JSONAbstractOrderedLayout;
import es.uva.tfg.generadorvistas.translater.JSONTranslater;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;

public class JSONTranslaterTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(JSONTranslaterTest.class);

	private static JsonNode horizontalLayoutJson;

	@BeforeClass
	public static void initJson() throws IOException {
		horizontalLayoutJson = JsonLoader
				.fromResource("/parser/examples/HorizontalLayout.json");
	}

	@Test
	public void translate() {
		LOG.info("Inicio del test del traductor de un JSON");
		JSONTranslater translater = new JSONTranslater();
		try {
			translater.setJson(horizontalLayoutJson);

			long timeIni = System.currentTimeMillis();
			JSONAbstractOrderedLayout jsonComponent = (JSONAbstractOrderedLayout) translater
					.translate();
			long timeFin = System.currentTimeMillis();

			double time = ((double) timeFin - timeIni) / 1000d;

			HorizontalLayout horizontalLayout = (HorizontalLayout) jsonComponent
					.getVaadinComponent();
			LOG.info("\tHorizontalLayout: " + horizontalLayout.toString());
			LOG.info("\tTiempo empleado en la traducción: " + time);

			LOG.info("Fin del test del traductor de un JSON");
		} catch (JSONValidationException e) {
			LOG.error("Error al traducir el JSON:\n " + e.getMessage());
		}

	}
}
