package es.uva.tfg.generadorvistas.serializator.exception;

public class ComponentNotSupportedException extends RuntimeException {

	private static final long serialVersionUID = -3830604030073103493L;

	private String message;

	public ComponentNotSupportedException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}

}
