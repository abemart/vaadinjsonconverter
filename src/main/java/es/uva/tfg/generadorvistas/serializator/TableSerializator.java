package es.uva.tfg.generadorvistas.serializator;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Component;
import com.vaadin.ui.Table;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeColumn;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeTable;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;

public class TableSerializator extends AbstractSerializator<JsonNodeTable> {

	@Override
	public JsonNodeTable serialize(Component component)
			throws ComponentNotSupportedException {
		super.serialize(component);

		Table table = (Table) component;

		// Inicializamos con las propiedades del JsonNodeTable
		JsonNodeTable jsonTable = new JsonNodeTable();
		jsonTable = initJsonNodeComponent(jsonTable, table);

		// Serializamos las columnas
		jsonTable = serializeColumns(table, jsonTable);

		return jsonTable;
	}

	@Override
	protected boolean checkClassesOk(Class<?> componentClass) {
		return componentClass.equals(Table.class);
	}

	/**
	 * Serializa las columnas de una tabla y las añade al JsonNodeTable.
	 * 
	 * @param table
	 *            La tabla de la cual queremos serializar las columnas.
	 * @param jsonNodeTable
	 *            El JsonNodeTable que corresponde a la tabla que queremos
	 *            serializar y al cual se le añaden las columnas serializadas.
	 * @return El JsonNodeTable con las columnas serializadas que contiene la
	 *         Table.
	 */
	private JsonNodeTable serializeColumns(Table table,
			JsonNodeTable jsonNodeTable) {
		List<JsonNodeColumn> jsonColumns = new ArrayList<>();
		Object[] visibleColumns = table.getVisibleColumns();
		for (Object propertyId : visibleColumns) {
			JsonNodeColumn jsonColumn = new JsonNodeColumn();
			jsonColumn.setHeader(table.getColumnHeader(propertyId));
			jsonColumn.setPropertyId(propertyId.toString());
			jsonColumns.add(jsonColumn);
		}

		jsonNodeTable.setColumns(jsonColumns);

		return jsonNodeTable;
	}

}
