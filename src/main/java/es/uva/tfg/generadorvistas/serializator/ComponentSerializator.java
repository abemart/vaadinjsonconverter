package es.uva.tfg.generadorvistas.serializator;

import com.vaadin.ui.Component;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeComponent;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;

/**
 * Interfaz que debe implementar todo Serializador de componentes Vaadin a
 * objetos JSON.
 * 
 * @param <T>
 *            El tipo de JsonNode en el que se va a serializar el componente
 *            Vaadin.
 * @since 1.0.0
 */
public interface ComponentSerializator<T extends JsonNodeComponent> {

	/**
	 * Seriazlia el componente Vaadin en formato JSON.
	 * 
	 * @param component
	 *            El componente de Vaadin que queremos serializar.
	 * @return El componete Vaadin serializado en JSON.
	 * 
	 * @throws ComponentNotSupportedException
	 *             Excepción que se lanza si el tipo del componente no está
	 *             admitido para su serialización.
	 */
	public abstract T serialize(Component component)
			throws ComponentNotSupportedException;

}
