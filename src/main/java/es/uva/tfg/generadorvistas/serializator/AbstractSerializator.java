package es.uva.tfg.generadorvistas.serializator;

import com.vaadin.ui.Component;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeComponent;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeHeight;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeWidth;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;

/**
 * Serializador por defecto que deben extender todos los Serializadores de
 * componentes. <br>
 * Este serializador tiene el inicializador de componentes base, en el cual se
 * inicializan las propiedades comunes a todos los componentes, es decir, las
 * propiedades de {@link JsonNodeComponent}.
 * 
 * @param <J>
 *            El tipo de {@link JsonNodeComponent} que corresponde con el
 *            componente Vaadin que se va a serializar.
 * @since 1.0.0
 */
public abstract class AbstractSerializator<J extends JsonNodeComponent>
		implements ComponentSerializator<J> {

	@Override
	public J serialize(Component component)
			throws ComponentNotSupportedException {
		Class<?> componentClass = component.getClass();

		// Comprobamos que es una clase admitida por nuestro framework
		if (!checkClassesOk(componentClass)) {
			throw new ComponentNotSupportedException(
					"La clase'" + componentClass.getName()
							+ "' no está soportada actualmente.");
		}
		
		return null;
	}

	/**
	 * Inicializa el componente JSON con las properties compartidas por todos
	 * los componentes, es decir las propiedades de {@link JsonNodeComponent}.
	 * 
	 * @param jsonComponent
	 *            El componente JSON que queremos inicializar.
	 * @param component
	 *            El componente de Vaadin del cual se van a obetener las
	 *            propiedades para inicializar el componente JSON.
	 * @return el componente JSON con las properties inicializadas.
	 */
	protected J initJsonNodeComponent(J jsonComponent, Component component) {
		jsonComponent.setId(component.getId());
		jsonComponent.setCaption(component.getCaption());
		jsonComponent.setEnabled(component.isEnabled());
		jsonComponent.setVisible(component.isVisible());
		jsonComponent.setReadOnly(component.isReadOnly());

		if (!component.getStyleName().isEmpty()) {
			jsonComponent.setStyleName(component.getStyleName());
		}

		if (component.getIcon() != null)
			jsonComponent.setIconPath(component.getIcon().toString());

		if (component.getWidth() != -1) {
			JsonNodeWidth width = new JsonNodeWidth();
			width.setWidth(component.getWidth());
			width.setUnit(component.getWidthUnits().getSymbol());
			jsonComponent.setWidth(width);
		}

		if (component.getHeight() != -1) {
			JsonNodeHeight height = new JsonNodeHeight();
			height.setHeight(component.getHeight());
			height.setUnit(component.getHeightUnits().getSymbol());
			jsonComponent.setHeight(height);
		}

		return jsonComponent;
	}

	protected abstract boolean checkClassesOk(Class<?> componentClass);

}
