package es.uva.tfg.generadorvistas.serializator;

import static es.uva.tfg.generadorvistas.parser.util.VaadinComponentUtil.*;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeAbstractOrderedLayout;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeAlignment;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeComponent;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeExpandRatio;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;
import es.uva.tfg.generadorvistas.translater.ComponentConverter;

public class AbstractOrderedLayoutSerializator
		extends AbstractSerializator<JsonNodeAbstractOrderedLayout> {

	@Override
	public JsonNodeAbstractOrderedLayout serialize(Component component)
			throws ComponentNotSupportedException {
		super.serialize(component);

		AbstractOrderedLayout orderedLayoutComponent = (AbstractOrderedLayout) component;

		Class<?> classComponent = component.getClass();
		JsonNodeAbstractOrderedLayout jsonComponent = new JsonNodeAbstractOrderedLayout(
				classComponent);
		jsonComponent = initJsonNodeComponent(jsonComponent,
				orderedLayoutComponent);

		jsonComponent.setMargin(hasMargin(orderedLayoutComponent.getMargin()));
		jsonComponent.setSpacing(orderedLayoutComponent.isSpacing());

		jsonComponent = serializeComponents(jsonComponent,
				orderedLayoutComponent);

		jsonComponent = setAlignments(jsonComponent, orderedLayoutComponent);
		jsonComponent = setExpandRatios(jsonComponent, orderedLayoutComponent);

		return jsonComponent;
	}

	@Override
	protected boolean checkClassesOk(Class<?> componentClass) {
		return componentClass.equals(HorizontalLayout.class)
				|| componentClass.equals(VerticalLayout.class);
	}

	/**
	 * Comprueba si el MarginInfo posee algún margen activado para saber si hay
	 * margen.
	 * 
	 * @param marginInfo
	 *            El MarginInfo del cual queremos obtener si hay margen.
	 * @return si el MarginInfo posee algún margen activado para saber si hay
	 *         margen.
	 */
	private boolean hasMargin(MarginInfo marginInfo) {
		return marginInfo.hasBottom() || marginInfo.hasLeft()
				|| marginInfo.hasRight() || marginInfo.hasRight()
				|| marginInfo.hasTop();
	}

	/**
	 * Serializa los componentes que contiene el AbstractOrderedLayout y los
	 * añade al JsonNodeAbstractOrderedLayout.
	 * 
	 * @param jsonComponent
	 *            El JsonNodeAbstractOrderedLayout al cual queremos añadir los
	 *            componentes que contiene el AbstractOrderedLayout.
	 * @param abstractOrderedLayout
	 *            El layout que contiene los componentes que queremos
	 *            serializar.
	 * @return El JsonNodeAbstractOrderedLayout con los componentes ya
	 *         serializados que contiene el AbstractOrderedLayout
	 *         correspondiente.
	 */
	private JsonNodeAbstractOrderedLayout serializeComponents(
			JsonNodeAbstractOrderedLayout jsonComponent,
			AbstractOrderedLayout abstractOrderedLayout) {
		ComponentConverter converter = new ComponentConverter();
		List<JsonNodeComponent> components = new ArrayList<>();

		for (Component c : abstractOrderedLayout) {
			JsonNodeComponent j = converter.convert(c);
			components.add(j);
		}

		jsonComponent.setComponents(components);
		return jsonComponent;
	}

	/**
	 * Obtiene todos los alignments de los componentes que contiene el
	 * AbstractOrderedLayout.
	 * 
	 * @param jsonComponent
	 *            El JsonNodeAbstractOrderedLayout que queremos serializar y al
	 *            cual se le van a añadir los alignments encontrados.
	 * @param abstractOrderedLayout
	 *            El AbstractOrderedLayout que contiene los componentes de los
	 *            cuales queremos obtener los alignments.
	 * @return Devuelve el JsonNodeAbstractOrderedLayout con los alignments de
	 *         los componentes que contiene.
	 */
	private JsonNodeAbstractOrderedLayout setAlignments(
			JsonNodeAbstractOrderedLayout jsonComponent,
			AbstractOrderedLayout abstractOrderedLayout) {
		List<JsonNodeAlignment> alignments = new ArrayList<>();
		for (Component c : abstractOrderedLayout) {
			JsonNodeAlignment jsonAlignment = getJsonNodeAlignment(c,
					abstractOrderedLayout);
			if (jsonAlignment != null)
				alignments.add(jsonAlignment);
		}

		if (!alignments.isEmpty())
			jsonComponent.setAlignments(alignments);
		return jsonComponent;
	}

	/**
	 * Obtiene todos los expandRatio de los componentes que contiene el
	 * AbstractOrderedLayout.
	 * 
	 * @param jsonComponent
	 *            El JsonNodeAbstractOrderedLayout que queremos serializar y al
	 *            cual se le van a añadir los expandRatios encontrados.
	 * @param abstractOrderedLayout
	 *            El AbstractOrderedLayout que contiene los componentes de los
	 *            cuales queremos obtener los expandRatios.
	 * @return Devuelve el JsonNodeAbstractOrderedLayout con los expandRatios de
	 *         los componentes que contiene.
	 */
	private JsonNodeAbstractOrderedLayout setExpandRatios(
			JsonNodeAbstractOrderedLayout jsonComponent,
			AbstractOrderedLayout abstractOrderedLayout) {
		List<JsonNodeExpandRatio> expandRatios = new ArrayList<>();
		for (Component c : abstractOrderedLayout) {
			JsonNodeExpandRatio jsonExpandRatio = getJsonNodeExpandRatio(c,
					abstractOrderedLayout);
			if (jsonExpandRatio != null)
				expandRatios.add(jsonExpandRatio);
		}

		if (!expandRatios.isEmpty())
			jsonComponent.setExpandRatio(expandRatios);
		return jsonComponent;
	}

	/**
	 * Obtiene el JsonNodeAlignment a partir del componente que posee el
	 * alignment y el AbstractOrderedLayout que contiene al componente.
	 * 
	 * @param component
	 *            El componente del cual queremos obtener el alignment.
	 * @param abstractOrderedLayout
	 *            El layout que contiene al componente del cual queremos obtener
	 *            el alignment.
	 * @return El JsonNodeAlignment del componente indicado qeu posee en el
	 *         AbstractOrderedLayout. Si el componente no posee 'id' devuelve
	 *         null.
	 */
	private JsonNodeAlignment getJsonNodeAlignment(Component component,
			AbstractOrderedLayout abstractOrderedLayout) {
		JsonNodeAlignment jsonAlignment = null;
		if (component.getId() != null) {
			Alignment alignment = abstractOrderedLayout
					.getComponentAlignment(component);
			jsonAlignment = new JsonNodeAlignment();
			jsonAlignment.setComponentId(component.getId());
			jsonAlignment.setAlignment(getAlignment(alignment));
		}

		return jsonAlignment;
	}

	/**
	 * Obtiene el JsonNodeExpandRatio a partir del componente que posee el
	 * expand ratio y el AbstractOrderedLayout que contiene al componente.
	 * 
	 * @param component
	 *            El componente del cual queremos obtener el expand ratio.
	 * @param abstractOrderedLayout
	 *            El layout que contiene al componente del cual queremos obtener
	 *            el expand ratio.
	 * @return El JsonNodeExpandRatio del componente indicado que posee en el
	 *         AbstractOrderedLayout. Si el componente no posee 'expandRatio' o
	 *         'id' devuelve null.
	 */
	private JsonNodeExpandRatio getJsonNodeExpandRatio(Component component,
			AbstractOrderedLayout abstractOrderedLayout) {
		JsonNodeExpandRatio jsonExpandRatio = null;
		Float expandRatio = abstractOrderedLayout.getExpandRatio(component);
		if (component.getId() != null && expandRatio != 0) {
			jsonExpandRatio = new JsonNodeExpandRatio();
			jsonExpandRatio.setComponentId(component.getId());
			jsonExpandRatio.setExpandRatio(expandRatio);
		}

		return jsonExpandRatio;
	}
}
