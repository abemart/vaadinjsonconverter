package es.uva.tfg.generadorvistas.serializator;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.server.Resource;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeMenuBar;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeMenuItem;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;

public class MenuBarSerializator extends AbstractSerializator<JsonNodeMenuBar> {

	@Override
	public JsonNodeMenuBar serialize(Component component)
			throws ComponentNotSupportedException {
		super.serialize(component);

		MenuBar menuBar = (MenuBar) component;

		// Inicializamos con las propiedades del JsonNodeComponent
		JsonNodeMenuBar jsonMenuBar = new JsonNodeMenuBar();
		jsonMenuBar = initJsonNodeComponent(jsonMenuBar, menuBar);

		// Serializamos los menu items
		jsonMenuBar = serializeMenuItems(menuBar, jsonMenuBar);

		return jsonMenuBar;
	}

	@Override
	protected boolean checkClassesOk(Class<?> componentClass) {
		return componentClass.equals(MenuBar.class);
	}

	/**
	 * Serializa los menuItems del MenuBar y los añade al JsonNodeMenuBar.
	 * 
	 * @param menuBar
	 *            El MenuBar del cual queremos serializar los menu items.
	 * @param jsonNodeMenuBar
	 *            El JsonNodeMenuBar correspondiente a la serialización del
	 *            MenuBar y al cual se van a añadir los menuItems serializados.
	 * @return El JsonNodeMenuBar con los menuItems serializados.
	 */
	private JsonNodeMenuBar serializeMenuItems(MenuBar menuBar,
			JsonNodeMenuBar jsonNodeMenuBar) {
		List<JsonNodeMenuItem> jsonMenuItems = new ArrayList<>();
		for (MenuItem menuItem : menuBar.getItems()) {
			jsonMenuItems.add(serializeMenuItem(menuItem));
		}

		if (!jsonMenuItems.isEmpty())
			jsonNodeMenuBar.setMenuItems(jsonMenuItems);

		return jsonNodeMenuBar;
	}

	/**
	 * Seriazlia un MenuItem.
	 * 
	 * @param menuItem
	 *            El MenuItem que queremos serializar.
	 * @return El MenuItem serializado.
	 */
	private JsonNodeMenuItem serializeMenuItem(MenuItem menuItem) {
		JsonNodeMenuItem jsonMenuItem = new JsonNodeMenuItem();
		jsonMenuItem.setText(menuItem.getText());
		jsonMenuItem.setEnabled(menuItem.isEnabled());
		jsonMenuItem.setSeparator(menuItem.isSeparator());
		jsonMenuItem.setVisible(menuItem.isVisible());
		jsonMenuItem.setStyleName(menuItem.getStyleName());

		Resource icon = menuItem.getIcon();
		if (icon != null) {
			jsonMenuItem.setIconPath(icon.toString());
		}

		List<JsonNodeMenuItem> menuItemsChildren = new ArrayList<>();
		if (menuItem.getChildren() != null) {
			for (MenuItem m : menuItem.getChildren()) {
				menuItemsChildren.add(serializeMenuItem(m));
			}
		}

		if (!menuItemsChildren.isEmpty())
			jsonMenuItem.setMenuItems(menuItemsChildren);

		return jsonMenuItem;
	}

}
