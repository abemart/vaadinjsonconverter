package es.uva.tfg.generadorvistas.serializator.util;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import es.uva.tfg.generadorvistas.serializator.AbstractComponentSerializator;
import es.uva.tfg.generadorvistas.serializator.AbstractOrderedLayoutSerializator;
import es.uva.tfg.generadorvistas.serializator.ComponentSerializator;
import es.uva.tfg.generadorvistas.serializator.MenuBarSerializator;
import es.uva.tfg.generadorvistas.serializator.TabSheetSerializator;
import es.uva.tfg.generadorvistas.serializator.TableSerializator;

public class DecisionSerializator {

	public static ComponentSerializator<?> getSerializator(
			Component component) {
		ComponentSerializator<?> serializator = null;

		if (component == null)
			return serializator;

		Class<?> classComponent = component.getClass();

		if (classComponent.equals(HorizontalLayout.class)
				|| classComponent.equals(VerticalLayout.class)) {
			serializator = new AbstractOrderedLayoutSerializator();
		} else if (classComponent.equals(Button.class)
				|| classComponent.equals(TextField.class)
				|| classComponent.equals(DateField.class)
				|| classComponent.equals(CheckBox.class)
				|| classComponent.equals(ComboBox.class)) {
			serializator = new AbstractComponentSerializator();
		} else if (classComponent.equals(MenuBar.class)) {
			serializator = new MenuBarSerializator();
		} else if (classComponent.equals(Table.class)) {
			serializator = new TableSerializator();
		} else if (classComponent.equals(TabSheet.class)) {
			serializator = new TabSheetSerializator();
		}

		return serializator;
	}

}
