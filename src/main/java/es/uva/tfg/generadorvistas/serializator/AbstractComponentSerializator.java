package es.uva.tfg.generadorvistas.serializator;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeAbstractComponent;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;

/**
 * Serializador para los {@link AbstractComponent} de Vaadin.
 * 
 * @since 1.0.0
 */
public class AbstractComponentSerializator
		extends AbstractSerializator<JsonNodeAbstractComponent> {

	@Override
	public JsonNodeAbstractComponent serialize(Component component)
			throws ComponentNotSupportedException {
		super.serialize(component);

		AbstractComponent abstractComponent = (AbstractComponent) component;

		// Inicializamos con las propiedades del JsonNodeComponent
		Class<?> classComponent = component.getClass();
		JsonNodeAbstractComponent jsonComponent = new JsonNodeAbstractComponent(
				classComponent);
		jsonComponent = initJsonNodeComponent(jsonComponent, abstractComponent);

		return jsonComponent;
	}

	@Override
	protected boolean checkClassesOk(Class<?> componentClass) {
		return componentClass.equals(Button.class)
				|| componentClass.equals(TextField.class)
				|| componentClass.equals(CheckBox.class)
				|| componentClass.equals(DateField.class)
				|| componentClass.equals(ComboBox.class);
	}

}
