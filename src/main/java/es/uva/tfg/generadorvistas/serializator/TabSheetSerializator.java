package es.uva.tfg.generadorvistas.serializator;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.server.Resource;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeComponent;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeTab;
import es.uva.tfg.generadorvistas.components.serialization.JsonNodeTabSheet;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;
import es.uva.tfg.generadorvistas.translater.ComponentConverter;

public class TabSheetSerializator
		extends AbstractSerializator<JsonNodeTabSheet> {

	@Override
	public JsonNodeTabSheet serialize(Component component)
			throws ComponentNotSupportedException {
		super.serialize(component);

		TabSheet tabSheet = (TabSheet) component;

		// Inicializamos con las propiedades del JsonNodeTabSheet
		JsonNodeTabSheet jsonTabSheet = new JsonNodeTabSheet();
		jsonTabSheet = initJsonNodeComponent(jsonTabSheet, component);

		// Serializamos las tabs
		jsonTabSheet = serializeTabs(tabSheet, jsonTabSheet);

		return jsonTabSheet;
	}

	@Override
	protected boolean checkClassesOk(Class<?> componentClass) {
		return componentClass.equals(TabSheet.class);
	}

	/**
	 * Serializa las tabs del TabSheet.
	 * 
	 * @param tabSheet
	 *            El TabSheet que contiene las Tabs que queremos serializar.
	 * @param jsonTabSheet
	 *            El JsonTabSheet que corresponde con el TabSheet a serializar
	 *            al cual se van a añadir las Tabs serializadas.
	 * @return El JsonTabSheet con las Tabs ya serializadas.
	 */
	private JsonNodeTabSheet serializeTabs(TabSheet tabSheet,
			JsonNodeTabSheet jsonTabSheet) {
		int numTabs = tabSheet.getComponentCount();

		List<JsonNodeTab> tabs = new ArrayList<>();
		for (int i = 0; i < numTabs; i++) {
			Tab tab = tabSheet.getTab(i);
			JsonNodeTab jsonTab = new JsonNodeTab();
			jsonTab.setCaption(tab.getCaption());

			Resource icon = tab.getIcon();
			if (icon != null)
				jsonTab.setIcon(icon.toString());

			Component content = tab.getComponent();
			if (content != null) {
				ComponentConverter converter = new ComponentConverter();
				JsonNodeComponent jsonContent = converter.convert(content);
				jsonTab.setContent(jsonContent);
			}

			tabs.add(jsonTab);
		}

		if (!tabs.isEmpty())
			jsonTabSheet.setTabs(tabs);

		return jsonTabSheet;
	}
}
