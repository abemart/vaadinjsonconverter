package es.uva.tfg.generadorvistas.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.ui.Component;

import es.uva.tfg.generadorvistas.components.JSONComponent;

/**
 * Interfaz que deben implementar todos los parsers de Vaadin Json Converter.
 * 
 * @param <E>
 *            Tipo del componente Vaadin que se va a parsear.
 * @param <T>
 *            Tipo del {@link JSONComponent} que se va a parsear.
 * 
 * @since 1.0.0
 */
public abstract class JSONParser<E extends Component, T extends JSONComponent<E>> {

	/**
	 * Parsea un JSON a un {@link JSONComponent}.
	 * 
	 * @param json
	 *            El JSON que queremos parsear.
	 * @return el JSONComponent que se a obtenido al parsear el JSON indicado.
	 */
	public abstract T parse(JsonNode json);

	/**
	 * Crea un {@link Component} de Vaadin a partir del JSON indicado.
	 * 
	 * @param json
	 *            El JSON que queremos convertir a un componente de Vaadin.
	 * @return el {@link Component} de Vaadin que se ha obtenido al parsear el
	 *         JSON.
	 */
	protected abstract E createVaadinComponent(JsonNode json);

}
