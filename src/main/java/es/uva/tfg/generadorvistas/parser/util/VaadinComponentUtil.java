package es.uva.tfg.generadorvistas.parser.util;

import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import es.uva.tfg.generadorvistas.components.util.JSONField;
import es.uva.tfg.generadorvistas.components.util.JSONType;

public class VaadinComponentUtil {

	/**
	 * Crea el componente de Vaadin que corresponde con el JSON que se le
	 * indica.
	 * 
	 * @param json
	 *            El JSON que queremos convertir a componente de Vaadin.
	 * @return el componente de Vaadin que corresponde con el JSON.
	 */
	public static Component createVaadinComponent(JsonNode json) {
		Component component = null;

		if (json != null) {
			// Obtenemos el tipo del JSON y creamos el componente a partir del
			// tipo que hemos obtenido
			JSONType type = DecisionParser.getType(json);
			component = createComponentByType(type);
			component = setVaadinProperties(json, component);
		}

		return component;
	}

	/**
	 * Crea el {@link MenuItem} correspondiente al JSON que se le indica y se lo
	 * añade al menuBar.
	 * 
	 * @param menuBar
	 *            El MenuBar al cual corresponde el {@link MenuItem}.
	 * @param json
	 *            El json que corresponde al MenuItem que queremos crear.
	 * @return el {@link MenuItem} correspondiente al JSON que se le indica y se
	 *         lo añade al menuBar.
	 */
	public static MenuItem createMenuItem(MenuBar menuBar, JsonNode json) {
		return parseMenuItem(menuBar, null, json);
	}

	/**
	 * Obtiene el {@link Alignment} que corresponde con el String indicado.
	 * 
	 * @param alignment
	 *            El alignment en formato String.
	 * @return el {@link Alignment} que corresponde con el String indicado.
	 */
	public static Alignment getAlignment(String alignment) {
		Alignment align = null;

		if (alignment != null) {
			if (alignment.equals("TOP_LEFT")) {
				align = Alignment.TOP_LEFT;
			} else if (alignment.equals("TOP_CENTER")) {
				align = Alignment.TOP_CENTER;
			} else if (alignment.equals("TOP_RIGHT")) {
				align = Alignment.TOP_RIGHT;
			} else if (alignment.equals("MIDDLE_LEFT")) {
				align = Alignment.MIDDLE_LEFT;
			} else if (alignment.equals("MIDDLE_CENTER")) {
				align = Alignment.MIDDLE_CENTER;
			} else if (alignment.equals("MIDDLE_RIGHT")) {
				align = Alignment.MIDDLE_RIGHT;
			} else if (alignment.equals("BOTTOM_LEFT")) {
				align = Alignment.BOTTOM_LEFT;
			} else if (alignment.equals("BOTTOM_CENTER")) {
				align = Alignment.BOTTOM_CENTER;
			} else if (alignment.equals("BOTTOM_RIGHT")) {
				align = Alignment.BOTTOM_RIGHT;
			}
		}
		return align;
	}

	/**
	 * Obtiene el String que corresponde con el {@link Alignment} indicado.
	 * 
	 * @param alignment
	 *            El alignment que queremos transformar a String.
	 * @return el String que corresponde con el {@link Alignment} indicado.
	 */
	public static String getAlignment(Alignment alignment) {
		String align = null;

		if (alignment != null) {
			if (alignment.equals(Alignment.TOP_LEFT)) {
				align = "TOP_LEFT";
			} else if (alignment.equals(Alignment.TOP_CENTER)) {
				align = "TOP_CENTER";
			} else if (alignment.equals(Alignment.TOP_RIGHT)) {
				align = "TOP_RIGHT";
			} else if (alignment.equals(Alignment.MIDDLE_LEFT)) {
				align = "MIDDLE_LEFT";
			} else if (alignment.equals(Alignment.MIDDLE_CENTER)) {
				align = "MIDDLE_CENTER";
			} else if (alignment.equals(Alignment.MIDDLE_RIGHT)) {
				align = "MIDDLE_RIGHT";
			} else if (alignment.equals(Alignment.BOTTOM_LEFT)) {
				align = "BOTTOM_LEFT";
			} else if (alignment.equals(Alignment.BOTTOM_CENTER)) {
				align = "BOTTOM_CENTER";
			} else if (alignment.equals(Alignment.BOTTOM_RIGHT)) {
				align = "BOTTOM_RIGHT";
			}
		}
		return align;
	}

	/**
	 * Aplica las propiedades básicas y principales de Vaadin, al componete de
	 * Vaadin que posee el JSON.
	 * 
	 * @param json
	 *            El JSON que corresponde con el componente Vaadin al cual
	 *            queremos aplicar las propiedades.
	 * @param vaadinComponent
	 *            El componente de Vaadin al cual se le quieren aplicar las
	 *            propiedades.
	 * @return devuelve el componete de Vaadin con las propiedades aplicadas que
	 *         se han encontrado en el JSON.
	 */
	private static Component setVaadinProperties(JsonNode json,
			Component vaadinComponent) {
		// Obtenemos el elemento JSON
		json = json.elements().next();

		JsonNode idJson = json.get(JSONField.ID.getField());
		String id = idJson != null ? idJson.textValue() : null;

		JsonNode captionJson = json.get(JSONField.CAPTION.getField());
		String caption = captionJson != null ? captionJson.textValue() : null;

		JsonNode widthJson = json.get(JSONField.WIDTH.getField());
		JsonNode heightJson = json.get(JSONField.HEIGHT.getField());
		setSize(widthJson, JSONField.WIDTH, vaadinComponent);
		setSize(heightJson, JSONField.HEIGHT, vaadinComponent);

		JsonNode iconJson = json.get(JSONField.ICON.getField());
		String icon = iconJson != null ? iconJson.textValue() : null;

		JsonNode styleNameJson = json.get(JSONField.STYLE_NAME.getField());
		String styleName = styleNameJson != null ? styleNameJson.textValue()
				: null;

		JsonNode enabledJson = json.get(JSONField.ENABLED.getField());
		Boolean enabled = enabledJson != null ? enabledJson.booleanValue()
				: null;

		JsonNode readOnlyJson = json.get(JSONField.READ_ONLY.getField());
		Boolean readOnly = readOnlyJson != null ? readOnlyJson.booleanValue()
				: null;

		JsonNode visibleJson = json.get(JSONField.VISIBLE.getField());
		Boolean visible = visibleJson != null ? visibleJson.booleanValue()
				: null;

		if (id != null)
			vaadinComponent.setId(id);
		if (caption != null)
			vaadinComponent.setCaption(caption);
		if (icon != null)
			vaadinComponent.setIcon(new ThemeResource(icon));
		if (styleName != null)
			vaadinComponent.addStyleName(styleName);
		if (enabled != null)
			vaadinComponent.setEnabled(enabled);
		if (readOnly != null)
			vaadinComponent.setReadOnly(readOnly);
		if (visible != null)
			vaadinComponent.setVisible(visible);

		return vaadinComponent;
	}

	private static void setSize(JsonNode jsonSize, JSONField field,
			Component vaadinComponent) {
		if (jsonSize != null && field != null && vaadinComponent != null) {
			if (field.equals(JSONField.WIDTH)) {
				JsonNode widthJson = jsonSize.get(JSONField.WIDTH.getField());
				JsonNode widthUnitJson = jsonSize
						.get(JSONField.SIZE_UNIT.getField());

				// Obtenemos la anchura y la unidad. Si no tiene unidad px por
				// defecto
				float width = widthJson != null ? widthJson.floatValue() : -1;
				String unit = widthUnitJson != null ? widthUnitJson.textValue()
						: "px";

				vaadinComponent.setWidth(width + unit);
			} else if (field.equals(JSONField.HEIGHT)) {
				JsonNode heightJson = jsonSize.get(JSONField.HEIGHT.getField());
				JsonNode heightUnitJson = jsonSize
						.get(JSONField.SIZE_UNIT.getField());

				// Obtenemos la altura y la unidad, si no tiene unidad px por
				// defecto
				float height = heightJson != null ? heightJson.floatValue()
						: -1;
				String unit = heightUnitJson != null
						? heightUnitJson.textValue() : "px";

				vaadinComponent.setHeight(height + unit);
			}
		}
	}

	private static Component createComponentByType(JSONType type) {
		Component component = null;
		switch (type) {
			case HORIZONTAL_LAYOUT:
				component = new HorizontalLayout();
				break;
			case VERTICAL_LAYOUT:
				component = new VerticalLayout();
				break;
			case TAB_SHEET:
				component = new TabSheet();
				break;
			case MENU_BAR:
				component = new MenuBar();
				break;
			case BUTTON:
				component = new Button();
				break;
			case CHECKBOX:
				component = new CheckBox();
				break;
			case TEXT_FIELD:
				component = new TextField();
				break;
			case DATE_FIELD:
				component = new DateField();
				break;
			case COMBO_BOX:
				component = new ComboBox();
				break;
			case TABLE:
				component = new Table();
				break;
			default:
				break;
		}

		return component;
	}

	private static MenuItem parseMenuItem(MenuBar menuBar, MenuItem menuItem,
			JsonNode jsonMenuItem) {
		MenuItem item = null;

		if (jsonMenuItem != null) {
			JsonNode textJson = jsonMenuItem.get(JSONField.TEXT.getField());
			JsonNode iconJson = jsonMenuItem.get(JSONField.ICON.getField());
			JsonNode separatorJson = jsonMenuItem
					.get(JSONField.SEPARATOR.getField());
			JsonNode enabledJson = jsonMenuItem
					.get(JSONField.ENABLED.getField());
			JsonNode visibleJson = jsonMenuItem
					.get(JSONField.VISIBLE.getField());
			JsonNode menuItemsJson = jsonMenuItem
					.get(JSONField.MENU_ITEMS.getField());

			String text = "", iconPath;
			Boolean separator, enabled, visible;

			if (menuBar != null) {
				item = menuBar.addItem(text, null);
			} else {
				item = menuItem.addItem(text, null);
			}

			if (textJson != null) {
				text = textJson.asText();
				item.setText(text);
			}

			if (iconJson != null) {
				iconPath = iconJson.asText();
				item.setIcon(new ThemeResource(iconPath));
			}

			if (separatorJson != null) {
				separator = separatorJson.asBoolean();
				if (separator)
					item.addSeparator();
			}

			if (enabledJson != null) {
				enabled = enabledJson.asBoolean();
				item.setEnabled(enabled);
			}

			if (visibleJson != null) {
				visible = visibleJson.asBoolean();
				item.setVisible(visible);
			}

			if (menuItemsJson != null) {
				Iterator<JsonNode> it = menuItemsJson.iterator();
				while (it.hasNext()) {
					JsonNode menuItemChild = it.next();
					parseMenuItem(null, item, menuItemChild);
				}
			}
		}

		return item;
	}

}
