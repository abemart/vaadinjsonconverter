package es.uva.tfg.generadorvistas.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.ui.AbstractComponent;

import es.uva.tfg.generadorvistas.components.JSONAbstractComponent;
import es.uva.tfg.generadorvistas.parser.util.VaadinComponentUtil;

/**
 * Parser que traduce JSON's que corresponden con componentes Vaadin de tipo
 * {@link AbstractComponent}.
 * 
 * @since 1.0.0
 */
public class AbstractComponentParser
		extends JSONParser<AbstractComponent, JSONAbstractComponent> {

	@Override
	public JSONAbstractComponent parse(JsonNode json) {
		JSONAbstractComponent jsonComponent = new JSONAbstractComponent();
		jsonComponent.setJson(json);

		// Creamos el componente Vaadin que corresponde con el JSON indicado
		AbstractComponent vaadinComponent = createVaadinComponent(json);

		// Una vez obtenido el componete y con las propiedades seteadas, se lo
		// añadimos al JSONComponent
		jsonComponent.setVaadinComponent(vaadinComponent);

		// Obtenemos el id y se lo asignamos al json component
		String id = vaadinComponent.getId();
		jsonComponent.setId(id);

		return jsonComponent;
	}

	@Override
	protected AbstractComponent createVaadinComponent(JsonNode json) {
		AbstractComponent vaadinComponent = null;

		vaadinComponent = (AbstractComponent) VaadinComponentUtil
				.createVaadinComponent(json);

		return vaadinComponent;
	}

}
