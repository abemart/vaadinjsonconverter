package es.uva.tfg.generadorvistas.parser.util;

import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;

import es.uva.tfg.generadorvistas.components.util.JSONType;
import es.uva.tfg.generadorvistas.parser.AbstractComponentParser;
import es.uva.tfg.generadorvistas.parser.AbstractOrderedLayoutParser;
import es.uva.tfg.generadorvistas.parser.JSONParser;
import es.uva.tfg.generadorvistas.parser.MenuBarParser;
import es.uva.tfg.generadorvistas.parser.TabSheetParser;
import es.uva.tfg.generadorvistas.parser.TableParser;

public class DecisionParser {

	/**
	 * Obtiene el {@link JSONType} del componente JSON.
	 * 
	 * @param json
	 *            El JSON del cual queremos obtener el tipo.
	 * @return el tipo del componente JSON. Si no coincide con ningún tipo
	 *         disponible devuelve <code>null</code>.
	 */
	public static JSONType getType(JsonNode json) {
		Iterator<String> it = json.fieldNames();
		String typeStr = null;

		while (it.hasNext()) {
			typeStr = it.next();
			break;
		}

		return JSONType.fromString(typeStr);
	}

	/**
	 * Obtiene el {@link JSONParser} adecuado según el tipo del JSON.
	 * 
	 * @param json
	 *            El JSON para el cual queremos obtener su parser.
	 * @return el JSONParser adecuado para el tipo de JSON.
	 */
	public static JSONParser<?, ?> getParser(JsonNode json) {
		JSONParser<?, ?> parser = null;

		if (json != null) {
			JSONType type = getType(json);
			switch (type) {
				case HORIZONTAL_LAYOUT:
				case VERTICAL_LAYOUT:
					parser = new AbstractOrderedLayoutParser();
					break;
				case BUTTON:
				case CHECKBOX:
				case TEXT_FIELD:
				case DATE_FIELD:
				case COMBO_BOX:
					parser = new AbstractComponentParser();
					break;
				case MENU_BAR:
					parser = new MenuBarParser();
					break;
				case TABLE:
					parser = new TableParser();
					break;
				case TAB_SHEET:
					parser = new TabSheetParser();
					break;
				default:
					break;
			}
		}

		return parser;
	}

}
