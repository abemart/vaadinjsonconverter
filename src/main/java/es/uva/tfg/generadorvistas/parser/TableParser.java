package es.uva.tfg.generadorvistas.parser;

import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.ui.Table;

import es.uva.tfg.generadorvistas.components.JSONTableComponent;
import es.uva.tfg.generadorvistas.components.util.JSONField;
import es.uva.tfg.generadorvistas.parser.util.VaadinComponentUtil;

/**
 * Parser que traduce JSON's que corresponden con componentes Vaadin de tipo
 * {@link Table}.
 * 
 * @since 1.0.0
 */
public class TableParser extends JSONParser<Table, JSONTableComponent> {

	@Override
	public JSONTableComponent parse(JsonNode json) {
		JSONTableComponent jsonTable = parseTable(json);
		Table table = jsonTable.getVaadinComponent();

		// Parseamos las columnas
		parseColumns(table, json);

		return jsonTable;
	}

	@Override
	protected Table createVaadinComponent(JsonNode json) {
		Table table = null;

		if (json != null) {
			table = (Table) VaadinComponentUtil.createVaadinComponent(json);
		}
		return table;
	}

	private JSONTableComponent parseTable(JsonNode json) {
		JSONTableComponent jsonTable = new JSONTableComponent();
		jsonTable.setJson(json);

		// Obtenemos la tabla que corresponde
		Table table = createVaadinComponent(json);

		// Obtenemos el id
		String idTable = table.getId();
		jsonTable.setId(idTable);

		// Le indicamos al componente JSON la tabla creada
		jsonTable.setVaadinComponent(table);

		return jsonTable;
	}

	private void parseColumns(Table table, JsonNode json) {
		// Obtenemos el objeto concreto
		json = json.elements().next();

		JsonNode jsonColumns = json.get(JSONField.COLUMNS.getField());
		if (jsonColumns != null) {
			Iterator<JsonNode> it = jsonColumns.iterator();
			while (it.hasNext()) {
				JsonNode column = it.next();

				JsonNode propertyJson = column
						.get(JSONField.PROPERTY.getField());
				JsonNode headerJson = column.get(JSONField.HEADER.getField());
				JsonNode typeColumnJson = column
						.get(JSONField.COLUMN_TYPE.getField());
				JsonNode defaultValueJson = column
						.get(JSONField.DEFAULT_VALUE.getField());

				String propertyStr = propertyJson.asText();
				String typeColumnStr = typeColumnJson.asText();
				String headerStr = headerJson != null ? headerJson.asText()
						: null;
				String defaultValueStr = defaultValueJson != null
						? defaultValueJson.asText() : null;

				table.addContainerProperty(propertyStr,
						getTypeClass(typeColumnStr), defaultValueStr);

				if (headerStr != null) {
					table.setColumnHeader(propertyStr, headerStr);
				}
			}
		}
	}

	/**
	 * Convierte el tipo en formato String a una clase de Java. Por defecto si
	 * no coincide con ninguno de los aceptados devuelve String.
	 * 
	 * @param typeStr
	 *            El tipo en formato String.
	 * @return El tipo Java al que corresponde el tipo en formato String. Si no
	 *         coincide con ninguno de los permitidos devuelve tipo String.
	 */
	private Class<?> getTypeClass(String typeStr) {
		Class<?> type = String.class;

		switch (typeStr) {
			case "String":
				type = String.class;
				break;
			case "Integer":
				type = Integer.class;
				break;
			case "Float":
				type = Float.class;
				break;
			case "Double":
				type = Double.class;
				break;
			case "Boolean":
				type = Boolean.class;
				break;
			default:
				type = String.class;
				break;
		}

		return type;
	}
}
