package es.uva.tfg.generadorvistas.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;

import es.uva.tfg.generadorvistas.components.JSONAbstractOrderedLayout;
import es.uva.tfg.generadorvistas.components.JSONComponent;
import es.uva.tfg.generadorvistas.components.util.JSONField;
import es.uva.tfg.generadorvistas.parser.util.VaadinComponentUtil;
import es.uva.tfg.generadorvistas.translater.JSONConverter;

/**
 * Parser que traduce JSON's que corresponden con componentes Vaadin de tipo
 * {@link AbstractOrderedLayout}.
 * 
 * @since 1.0.0
 */
public class AbstractOrderedLayoutParser
		extends JSONParser<AbstractOrderedLayout, JSONAbstractOrderedLayout> {

	@Override
	public JSONAbstractOrderedLayout parse(JsonNode json) {
		JSONAbstractOrderedLayout jsonComponentLayout = parseLayout(json);
		List<JSONComponent<?>> jsonComponents = parseChilds(json);

		// Obtenemos los posibles alineamientos y expands ratios
		Map<String, Alignment> alignments = getAlignments(json);
		Map<String, Long> expandRatios = getExpandRatio(json);

		// Añadimos los componentes que contiene el Abstract Ordered Layout
		// al componente principal
		for (JSONComponent<?> j : jsonComponents) {
			String id = j.getId();

			if (id != null) {
				// Obtenemos los posibles alignment y expand ratios
				Alignment alignment = alignments.get(id);
				Long expandRatio = expandRatios.get(id);

				jsonComponentLayout.addJSONComponent(j, alignment, expandRatio);
			} else {
				jsonComponentLayout.addJSONComponent(j);
			}
		}

		return jsonComponentLayout;
	}

	/**
	 * Obtiene el componente Vaadin de tipo {@link AbstractOrderedLayout} a
	 * partir del JSON que se le indica.
	 */
	@Override
	protected AbstractOrderedLayout createVaadinComponent(JsonNode json) {
		AbstractOrderedLayout layoutComponent = null;

		if (json != null) {
			layoutComponent = (AbstractOrderedLayout) VaadinComponentUtil
					.createVaadinComponent(json);
		}

		return layoutComponent;
	}

	/**
	 * Realiza la traducción del JSON principal, es decir del componente de tipo
	 * {@link AbstractOrderedLayout} a un {@link JSONAbstractOrderedLayout}.
	 * 
	 * @param json
	 *            El JSON a partir del cual se quiere crear el objeto
	 *            {@link JSONAbstractOrderedLayout}.
	 * @return Devuelve la traducción del JSON a JSONAbstractOrderedLayout.
	 */
	private JSONAbstractOrderedLayout parseLayout(JsonNode json) {
		JSONAbstractOrderedLayout jsonComponentLayout = new JSONAbstractOrderedLayout();
		jsonComponentLayout.setJson(json);

		// Obtenemos el layout de Vaadin correspondiente
		AbstractOrderedLayout layoutComponent = createVaadinComponent(json);

		// Le indicamos las propiedades propias de los Abstract Ordered Layout
		layoutComponent = setAbstractOrderedLayoutProperties(json,
				layoutComponent);

		// Obtenemos el ID si es que lo tiene para indicarselo al JSON Component
		String id = layoutComponent.getId();
		jsonComponentLayout.setId(id);

		// Le seteamos el componente de vaadin
		jsonComponentLayout.setVaadinComponent(layoutComponent);

		return jsonComponentLayout;
	}

	private List<JSONComponent<?>> parseChilds(JsonNode json) {
		List<JSONComponent<?>> jsonComponents = new ArrayList<>();
		JSONConverter converter = new JSONConverter();

		// Obtenemos el objeto concreto
		json = json.elements().next();

		JsonNode componentsJson = json.get(JSONField.COMPONENTS.getField());
		if (componentsJson != null) {
			Iterator<JsonNode> it = componentsJson.elements();
			while (it.hasNext()) {
				JsonNode component = it.next();

				// Convertimos el JSON
				JSONComponent<?> convertedComponent = converter
						.convert(component);

				// Añadimos el JSON componente al array de componentes
				jsonComponents.add(convertedComponent);
			}
		}

		return jsonComponents;
	}

	private Map<String, Alignment> getAlignments(JsonNode json) {
		Map<String, Alignment> alignments = new HashMap<>();

		// Obtenemos el objeto concreto
		json = json.elements().next();
		JsonNode alignmentsJson = json.get(JSONField.ALIGNMENTS.getField());

		if (alignmentsJson != null) {
			Iterator<JsonNode> it = alignmentsJson.elements();
			while (it.hasNext()) {
				JsonNode alignment = it.next();

				String componentId = alignment
						.get(JSONField.ALIGNMENT_COMPONENT_ID.getField())
						.asText();
				String typeAlignmentStr = alignment
						.get(JSONField.ALIGNMENT.getField()).asText();
				Alignment typeAlignment = VaadinComponentUtil
						.getAlignment(typeAlignmentStr);

				alignments.put(componentId, typeAlignment);
			}
		}

		return alignments;
	}

	private Map<String, Long> getExpandRatio(JsonNode json) {
		Map<String, Long> alignments = new HashMap<>();

		// Obtenemos el objeto concreto
		json = json.elements().next();
		JsonNode alignmentsJson = json.get(JSONField.EXPAND_RATIO.getField());

		if (alignmentsJson != null) {
			Iterator<JsonNode> it = alignmentsJson.elements();
			while (it.hasNext()) {
				JsonNode expandRatioJson = it.next();

				String componentId = expandRatioJson
						.get(JSONField.EXPAND_RATIO_COMPONENT_ID.getField())
						.asText();
				Long expandRatio = expandRatioJson
						.get(JSONField.EXPAND_RATIO.getField()).asLong();

				alignments.put(componentId, expandRatio);
			}
		}

		return alignments;
	}

	private AbstractOrderedLayout setAbstractOrderedLayoutProperties(
			JsonNode json, AbstractOrderedLayout component) {
		// Obtenemos el elemento JSON
		json = json.elements().next();

		JsonNode spacingJson = json.get(JSONField.SPACING.getField());
		Boolean spacing = spacingJson != null ? spacingJson.asBoolean() : null;

		JsonNode marginJson = json.get(JSONField.MARGIN.getField());
		Boolean margin = marginJson != null ? marginJson.asBoolean() : null;

		if (spacing != null)
			component.setSpacing(spacing);
		if (margin != null)
			component.setMargin(margin);

		return component;
	}

}
