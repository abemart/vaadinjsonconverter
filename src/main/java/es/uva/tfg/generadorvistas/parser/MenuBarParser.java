package es.uva.tfg.generadorvistas.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

import es.uva.tfg.generadorvistas.components.JSONMenuBarComponent;
import es.uva.tfg.generadorvistas.components.util.JSONField;
import es.uva.tfg.generadorvistas.parser.util.VaadinComponentUtil;

/**
 * Parser que traduce JSON's que corresponden con componentes Vaadin de tipo
 * {@link MenuBar}.
 * 
 * @since 1.0.0
 */
public class MenuBarParser extends JSONParser<MenuBar, JSONMenuBarComponent> {

	@Override
	public JSONMenuBarComponent parse(JsonNode json) {
		JSONMenuBarComponent jsonMenuBar = parseMenuBar(json);
		MenuBar menuBar = jsonMenuBar.getVaadinComponent();
		
		// Parseamos los posibles MenuItem's que pueda tener el MenuBar
		if (menuBar != null) 
			parseMenuItems(menuBar, json);
		
		return jsonMenuBar;
	}

	@Override
	protected MenuBar createVaadinComponent(JsonNode json) {
		MenuBar menuBar = null;

		if (json != null) {
			menuBar = (MenuBar) VaadinComponentUtil.createVaadinComponent(json);
		}
		return menuBar;
	}

	private JSONMenuBarComponent parseMenuBar(JsonNode json) {
		JSONMenuBarComponent jsonMenuBar = new JSONMenuBarComponent();
		jsonMenuBar.setJson(json);

		// Obtenemos el MenuBar correspondiente al JSON
		MenuBar menuBar = createVaadinComponent(json);

		// Obtenemos el ID el MenuBar para indicarselo al JSON
		String idMenuBar = menuBar.getId();
		jsonMenuBar.setId(idMenuBar);

		// Le indicamos el MenuBar al JSONComponent
		jsonMenuBar.setVaadinComponent(menuBar);

		return jsonMenuBar;
	}

	private List<MenuItem> parseMenuItems(MenuBar menuBar, JsonNode json) {
		List<MenuItem> menuItems = new ArrayList<>();

		// Obtenemos el objeto concreto
		json = json.elements().next();
		JsonNode menuItemsJson = json.get(JSONField.MENU_ITEMS.getField());

		if (menuItemsJson != null) {
			Iterator<JsonNode> it = menuItemsJson.iterator();
			while (it.hasNext()) {
				JsonNode menuItemJson = it.next();
				MenuItem menuItem = VaadinComponentUtil.createMenuItem(menuBar,
						menuItemJson);

				menuItems.add(menuItem);
			}
		}

		return menuItems;
	}

}
