package es.uva.tfg.generadorvistas.parser;

import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.TabSheet;

import es.uva.tfg.generadorvistas.components.JSONComponent;
import es.uva.tfg.generadorvistas.components.JSONTabSheetComponent;
import es.uva.tfg.generadorvistas.components.util.JSONField;
import es.uva.tfg.generadorvistas.parser.util.VaadinComponentUtil;
import es.uva.tfg.generadorvistas.translater.JSONConverter;

/**
 * Parser que traduce JSON's que corresponden con componentes Vaadin de tipo
 * {@link TabSheet}.
 * 
 * @since 1.0.0
 */
public class TabSheetParser
		extends JSONParser<TabSheet, JSONTabSheetComponent> {

	@Override
	public JSONTabSheetComponent parse(JsonNode json) {
		JSONTabSheetComponent jsonTabSheet = parseTabSheet(json);

		// Parseamos las tabs
		parseTabs(json, jsonTabSheet);

		return jsonTabSheet;
	}

	@Override
	protected TabSheet createVaadinComponent(JsonNode json) {
		TabSheet tabSheet = null;

		if (json != null) {
			tabSheet = (TabSheet) VaadinComponentUtil
					.createVaadinComponent(json);
		}
		return tabSheet;
	}

	private JSONTabSheetComponent parseTabSheet(JsonNode json) {
		JSONTabSheetComponent jsonTabSheet = new JSONTabSheetComponent();
		jsonTabSheet.setJson(json);

		TabSheet tabSheet = createVaadinComponent(json);

		String idTabSheet = tabSheet.getId();
		jsonTabSheet.setId(idTabSheet);

		jsonTabSheet.setVaadinComponent(tabSheet);
		return jsonTabSheet;
	}

	private void parseTabs(JsonNode json, JSONTabSheetComponent jsonTabSheet) {
		JSONConverter converter = new JSONConverter();

		// Obtenemos el objeto concreto
		json = json.elements().next();

		JsonNode jsonTabs = json.get(JSONField.TABS.getField());
		if (jsonTabs != null) {
			Iterator<JsonNode> it = jsonTabs.iterator();
			while (it.hasNext()) {
				JsonNode tab = it.next();

				JsonNode propertyCaption = tab
						.get(JSONField.CAPTION.getField());
				JsonNode propertyIcon = tab.get(JSONField.ICON.getField());
				JsonNode propertyContent = tab
						.get(JSONField.CONTENT.getField());

				String caption = propertyCaption.asText();
				String iconPath = propertyIcon != null ? propertyIcon.asText()
						: null;
				Resource icon = iconPath != null ? new ThemeResource(iconPath)
						: null;

				// Convertimos el componente del contenido de la tab
				JSONComponent<?> component = converter.convert(propertyContent);

				// Creamos la tab y la añadimos al tabSheet
				jsonTabSheet.addTabJSONComponent(component, caption, icon);
			}
		}
	}

}
