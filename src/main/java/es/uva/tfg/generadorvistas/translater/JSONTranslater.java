package es.uva.tfg.generadorvistas.translater;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;

import es.uva.tfg.generadorvistas.components.JSONComponent;
import es.uva.tfg.generadorvistas.translater.exception.JsonNotValidException;
import es.uva.tfg.generadorvistas.validator.JSONValidator;
import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;

/**
 * Clase encargada de traducir un objeto JSON compatible con los JSON que admite
 * Vaadin Json Converter. Todo JSON compatible será traducido a alguna de las
 * implementaciones de {@link JSONComponent} dependiendo del tipo del JSON.<br>
 * <br>
 * Antes de traducir un JSON, el traductor se encarga de comprobar si es un JSON
 * válido para el Framework. Si se tratase de un JSON no válido, en ningún caso
 * se realizaría una traducción.
 *
 * @since 1.0.0
 * 
 */
public class JSONTranslater {

	private static final Logger LOG = LoggerFactory
			.getLogger(JSONTranslater.class);

	private JSONConverter converter;
	private JSONValidator validator;

	private JsonNode json;

	public JSONTranslater() {
		converter = new JSONConverter();
		validator = JSONValidator.getInstance();
	}

	/**
	 * Traduce el JSON indicado a el {@link JSONComponent} adecuado. Para poder
	 * traducir el JSON antes se le ha tenido que indicar cual va a ser el JSON
	 * a traducir: {@link #setJson(JsonNode)}.
	 * 
	 * @return el JSONComponent que se ha generado a partir del JSON indicado.
	 */
	public JSONComponent<?> translate() {
		JSONComponent<?> jsonComponent = null;

		// Comprobamos que no sea nulo el json
		if (json != null) {
			jsonComponent = converter.convert(json);
		}

		return jsonComponent;
	}

	/**
	 * Obtiene el JSON que se le ha indicado al traductor que desea traducir.
	 * 
	 * @return el JSON que se le ha indicado al traductor que desea traducir.
	 */
	public JsonNode getJson() {
		return json;
	}

	/**
	 * Indica al traductor que JSON se quiere traducir.
	 * 
	 * @param json
	 *            El {@link JsonNode} que se desea traducir.
	 * @throws JSONValidationException
	 *             Excepción que se lanza si el JSON posee un formato inadecuado
	 *             que el framework sabe interpretar.
	 */
	public void setJson(JsonNode json) throws JSONValidationException {
		this.json = json;

		// Comprobamos que el Json es válido para el framework
		validator.validate(this.json);
	}

	/**
	 * Indica al traductor el JSON en formato String que se desea traducir.
	 * 
	 * @param json
	 *            El JSON en formato String que se quiere traducir.
	 * @throws JsonNotValidException
	 *             Excepción que se lanza si el JSON en formato String no se
	 *             puede transformar a objeto JSON.
	 * @throws JSONValidationException
	 *             Excepción que se lanza si el JSON posee un formato inadecuado
	 *             que el framework sabe interpretar.
	 */
	public void setJson(String json)
			throws JsonNotValidException, JSONValidationException {
		try {
			// Convertimos el String a JsonNoce
			this.json = JsonLoader.fromString(json);

			// Comprobamos que el Json es válido para el framework
			validator.validate(this.json);
		} catch (IOException e) {
			LOG.error("El json no tiene el formato adecuado.\n "
					+ e.getMessage());
			throw new JsonNotValidException(e.getMessage());
		}
	}

}
