package es.uva.tfg.generadorvistas.translater;

import com.fasterxml.jackson.databind.JsonNode;

import es.uva.tfg.generadorvistas.components.JSONComponent;
import es.uva.tfg.generadorvistas.parser.JSONParser;
import es.uva.tfg.generadorvistas.parser.util.DecisionParser;

/**
 * Clase encargada de convertir un JSON a el {@link JSONComponent} adecuado
 * según el tipo de este. <br>
 * <br>
 * El converter utiliza el {@link JSONParser} adecuado según el tipo de JSON que
 * se desea convertir. Si no se encuentra ningún parser adecuado para el tipo de
 * JSON correspondiente, la conversión no tendrá lugar.
 * 
 * @since 1.0.0
 * 
 */
public class JSONConverter {

	private JSONParser<?, ?> parser;

	/**
	 * Convierte el JSON a el {@link JSONComponent} adecuado según el tipo de
	 * este.
	 * 
	 * @param json
	 *            El JSON que queremos convertir a {@link JSONComponent}.
	 * @return el {@link JSONComponent} que se ha obtenido al traducir el JSON.
	 */
	public JSONComponent<?> convert(JsonNode json) {
		JSONComponent<?> jsonComponent = null;

		parser = DecisionParser.getParser(json);
		if (parser != null) {
			jsonComponent = parser.parse(json);
		}

		return jsonComponent;
	}

}
