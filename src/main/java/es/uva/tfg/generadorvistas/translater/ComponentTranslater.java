package es.uva.tfg.generadorvistas.translater;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vaadin.ui.Component;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeComponent;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;

public class ComponentTranslater {

	private Gson gson;
	private ComponentConverter converter;

	public ComponentTranslater() {
		this.converter = new ComponentConverter();
		this.gson = new GsonBuilder().setPrettyPrinting().create();
	}

	public String translate(Component component)
			throws ComponentNotSupportedException {
		String result = null;

		if (component != null) {
			JsonNodeComponent jsonComponent = converter.convert(component);
			result = gson.toJson(jsonComponent.getJsonObject());
		}

		return result;
	}

}
