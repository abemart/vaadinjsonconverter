package es.uva.tfg.generadorvistas.translater.exception;

/**
 * Excepción utilizada para cuando un JSON en formato String no tiene el formato
 * correcto para ser convertido a JSON.
 *
 * @since 1.0.0
 */
public class JsonNotValidException extends Exception {

	private static final long serialVersionUID = -1998592576925674506L;
	
	private String message;
	
	public JsonNotValidException(String message) {
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}

}
