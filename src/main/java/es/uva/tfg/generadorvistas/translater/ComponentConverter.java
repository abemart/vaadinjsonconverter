package es.uva.tfg.generadorvistas.translater;

import com.vaadin.ui.Component;

import static es.uva.tfg.generadorvistas.serializator.util.DecisionSerializator.*;

import es.uva.tfg.generadorvistas.components.serialization.JsonNodeComponent;
import es.uva.tfg.generadorvistas.serializator.ComponentSerializator;
import es.uva.tfg.generadorvistas.serializator.exception.ComponentNotSupportedException;

public class ComponentConverter {

	public JsonNodeComponent convert(Component component)
			throws ComponentNotSupportedException {
		JsonNodeComponent result = null;

		if (component != null) {
			ComponentSerializator<?> serializator = getSerializator(component);

			// Si no se ha encontrado un serializador es que no se puede
			// serializar este tipo de componentes
			if (serializator == null)
				throw new ComponentNotSupportedException(
						"No está soportada la serialización "
								+ "de componentes de tipo: "
								+ component.getClass().getSimpleName());

			result = serializator.serialize(component);
		}

		return result;
	}

}
