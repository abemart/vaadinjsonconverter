package es.uva.tfg.generadorvistas.validator.exceptions;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;

/**
 * Excepción que se lanza cuando el JSON no posee el formato correcto que
 * VaadinJsonConverter sabe interpretar.
 * 
 * @since 1.0.0
 */
public class JSONValidationException extends Exception {

	private static final long serialVersionUID = 716204108088777179L;

	private static final String REPORTS = "reports";

	private ProcessingReport report;

	public JSONValidationException(ProcessingReport report) {
		this.report = report;
	}

	@Override
	public String getMessage() {
		String mensaje = "";

		for (ProcessingMessage p : report) {
			mensaje += getMensajeError(p.asJson());
		}
		return mensaje;
	}

	private String getMensajeError(JsonNode error) {
		JsonNode reports = error.get(REPORTS);
		if (reports == null)
			return error.toString();

		return getReports(reports);
	}

	private String getReports(JsonNode reports) {
		String reportsStr = "";

		for (JsonNode r : reports) {
			r = r.get(0);
			if (r != null) {
				JsonNode report = r.get(REPORTS);

				if (report != null) {
					reportsStr += getReports(report);
				} else {
					JsonNode instance = r.get("instance") != null
							? r.get("instance").get("pointer") : r;

					reportsStr += "\n\tInstace: " + instance + "\n";
					reportsStr += "\tMessage: " + r.get("message");
				}
			}
		}

		return reportsStr;
	}
}
