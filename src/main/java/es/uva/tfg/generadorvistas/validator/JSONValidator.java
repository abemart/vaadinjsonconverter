package es.uva.tfg.generadorvistas.validator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import es.uva.tfg.generadorvistas.validator.exceptions.JSONValidationException;

/**
 * Clase encargada de validar que los JSON que se desean traducir a componentes
 * Vaadin son válidos para VaadinJsonConverter.
 * 
 * @since 1.0.0
 */
public class JSONValidator {

	private static final Logger LOG = LoggerFactory
			.getLogger(JSONValidator.class);

	private JsonSchema jsonSchema;
	private static JSONValidator instance;

	private JSONValidator() {
		JsonSchemaFactory factory = JsonSchemaFactory.byDefault();

		try {
			// Importante leer los ficheros con InputStream en lugar de File
			String line = null;
			String jsonSchemaStr = "";
			InputStream in = getClass()
					.getResourceAsStream("/schemas/AllComponentsSchema.json");
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			while ((line = reader.readLine()) != null) {
				jsonSchemaStr += line;
			}

			JsonNode jsonSchemaJson = JsonLoader.fromString(jsonSchemaStr);
			jsonSchema = factory.getJsonSchema(jsonSchemaJson);
		} catch (ProcessingException e) {
			LOG.error("No se ha podido inicializar el JsonSchema. Causa : \n"
					+ e.getMessage());
		} catch (IOException e) {
			LOG.error("No se ha podido inicializar el JsonSchema. Causa : \n"
					+ e.getMessage());
		}

		LOG.debug("Inicializado el JSONValidator");
	}

	public static JSONValidator getInstance() {
		if (instance == null) {
			instance = new JSONValidator();
		}

		return instance;
	}

	public boolean validate(JsonNode json) throws JSONValidationException {
		boolean ok = false;

		if (jsonSchema != null) {
			try {
				ProcessingReport report = jsonSchema.validate(json);
				
				if (report.isSuccess()) {
					ok = true;
				} else {
					throw new JSONValidationException(report);
				}
			} catch (ProcessingException e) {
				LOG.error("No se ha podido validar el JSON. Causa: \n"
						+ e.getMessage());
				ok = false;
			}
		}

		return ok;
	}

}
