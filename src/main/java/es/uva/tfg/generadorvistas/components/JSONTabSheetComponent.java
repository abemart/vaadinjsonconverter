package es.uva.tfg.generadorvistas.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vaadin.server.Resource;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;

/**
 * {@link JSONComponent} diseñado para contener componentes de Vaadin de tipo
 * {@link TabSheet}. Todos los componentes que se traducen a
 * {@link JSONTabSheetComponent} deben ser de tipo {@link TabSheet}.
 * 
 * @since 1.0.0
 */
public class JSONTabSheetComponent extends AbstractJSONComponent<TabSheet> {

	private List<JSONComponent<?>> listComponents;
	private Map<String, JSONComponent<?>> components;

	public JSONTabSheetComponent() {
		this.listComponents = new ArrayList<>();
		this.components = new HashMap<>();
	}

	/**
	 * Añade un nuevo {@link JSONComponent} como contenido de un tab del
	 * TabSheet.
	 * 
	 * @param jsonComponent
	 *            El JSONComponent que queremos añadir como Tab dentro del
	 *            TabSheet.
	 * @param caption
	 *            El caption que deseamos que tenga la Tab que va a contener al
	 *            componente.
	 * @param icon
	 *            El icono que deseamos que tenga la Tab que va a contener al
	 *            componente.
	 */
	public void addTabJSONComponent(JSONComponent<?> jsonComponent,
			String caption, Resource icon) {
		listComponents.add(jsonComponent);

		getVaadinComponent().addTab(jsonComponent.getVaadinComponent(), caption,
				icon);

		// Si el componente tiene id lo añadimos al mapa
		String id = jsonComponent != null ? jsonComponent.getId() : null;
		if (id != null) {
			components.put(id, jsonComponent);
		}
	}

	/**
	 * Obtiene el {@link JSONComponent} que coincide con el identificador y que
	 * se encuentra como JSONComponent entre los componentes del
	 * {@link JSONTabSheetComponent}.
	 */
	@Override
	public JSONComponent<?> getJSONComponent(String id) {
		JSONComponent<?> component = null;
		if (id != null) {
			if (getId() != null && getId().equals(id)) {
				component = this;
			} else {
				component = components.get(id);
			}
		}
		return component;
	}

	/**
	 * Obtiene el componente Vaadin que coincide con el identificador y que se
	 * encuentra como componente entre los componentes del
	 * {@link JSONTabSheetComponent}.
	 */
	@Override
	public Component getVaadinComponent(String id) {
		Component vaadinComponent = null;
		JSONComponent<?> jsonComponent = getJSONComponent(id);

		if (jsonComponent != null) {
			if (jsonComponent.getId().equals(id)) {
				vaadinComponent = jsonComponent.getVaadinComponent();
			}
		} else {
			for (JSONComponent<?> c : listComponents) {
				vaadinComponent = c.getVaadinComponent(id);
				if (vaadinComponent != null)
					return vaadinComponent;
			}
		}

		return vaadinComponent;
	}

}
