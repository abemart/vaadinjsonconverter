package es.uva.tfg.generadorvistas.components.util;

/**
 * Las propiedades que pueden poseer los JSON.
 * 
 * @since 1.0.0
 */
public enum JSONField {

	ID("id"),
	CAPTION("caption"),
	DESCRIPTION("description"),
	ICON("icon"),
	STYLE_NAME("styleName"),
	ENABLED("enabled"),
	TEXT("text"),
	SEPARATOR("separator"),
	READ_ONLY("readOnly"),
	VISIBLE("visible"),
	WIDTH("width"),
	HEIGHT("height"),
	SIZE_UNIT("unit"),
	SPACING("spacing"),
	MARGIN("margin"),
	COMPONENTS("components"),
	ALIGNMENTS("alignments"),
	ALIGNMENT("alignment"),
	ALIGNMENT_COMPONENT_ID("componentId"),
	EXPAND_RATIO("expandRatio"),
	EXPAND_RATIO_COMPONENT_ID("componentId"),
	MENU_ITEMS("menuItems"),
	COLUMNS("columns"),
	PROPERTY("propertyId"),
	HEADER("header"),
	COLUMN_TYPE("columnType"),
	DEFAULT_VALUE("defaultValue"),
	TABS("tabs"),
	CONTENT("content");

	private String field;

	JSONField(String field) {
		this.field = field;
	}
	
	public String getField() {
		return field;
	}
	
	public static JSONField fromString(String field) {
		if (field != null) {
			for (JSONField t : JSONField.values()) {
				if (field.equalsIgnoreCase(t.field)) {
					return t;
				}
			}
		}
		
		return null;
	}

}
