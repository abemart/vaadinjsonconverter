package es.uva.tfg.generadorvistas.components.util;

/**
 * Los tipos de componentes Vaadin que se pueden traducir de un JSON
 * 
 * @since 1.0.0
 */
public enum JSONType {

	HORIZONTAL_LAYOUT("HorizontalLayout"), 
	VERTICAL_LAYOUT("VerticalLayout"), 
	TAB_SHEET("TabSheet"), 
	MENU_BAR("MenuBar"), 
	BUTTON("Button"), 
	CHECKBOX("Checkbox"), 
	TEXT_FIELD("TextField"), 
	DATE_FIELD("DateField"), 
	COMBO_BOX("ComboBox"), 
	TABLE("Table"), 
	LABEL("Label"),
	MENU_ITEM("MenuItem");

	private String typeStr;

	private JSONType(String type) {
		this.typeStr = type;
	}
	
	public String getType() {
		return typeStr;
	}
	
	public static JSONType fromString(String type) {
		if (type != null) {
			for (JSONType t : JSONType.values()) {
				if (type.equalsIgnoreCase(t.typeStr)) {
					return t;
				}
			}
		}
		
		return null;
	}
}
