package es.uva.tfg.generadorvistas.components;

import com.vaadin.ui.MenuBar;

/**
 * {@link JSONComponent} diseñado para contener componentes de Vaadin de tipo
 * {@link MenuBar}. Todos los componentes que se traducen a
 * {@link JSONMenuBarComponent} deben ser de tipo {@link MenuBar}.
 * 
 * @since 1.0.0
 */
public class JSONMenuBarComponent extends AbstractJSONComponent<MenuBar> {

}
