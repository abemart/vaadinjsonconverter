package es.uva.tfg.generadorvistas.components.serialization;

import java.util.List;

/**
 * JsonNode que corresponde con los interpretación de los MenuItem de un
 * MenuBar.
 * 
 * @since 1.0.0
 */
public class JsonNodeMenuItem {

	private String styleName;
	private String text;
	private String iconPath;
	private Boolean enabled;
	private Boolean separator;
	private Boolean visible;
	private List<JsonNodeMenuItem> menuItems;

	public String getStyleName() {
		return styleName;
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getSeparator() {
		return separator;
	}

	public void setSeparator(Boolean separator) {
		this.separator = separator;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public List<JsonNodeMenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(List<JsonNodeMenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	@Override
	public String toString() {
		return "JsonNodeMenuItem [styleName=" + styleName + ", text=" + text
				+ ", iconPath=" + iconPath + ", enabled=" + enabled
				+ ", separator=" + separator + ", visible=" + visible
				+ ", menuItems=" + menuItems + "]";
	}

}
