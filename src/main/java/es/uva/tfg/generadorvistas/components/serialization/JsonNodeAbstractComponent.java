package es.uva.tfg.generadorvistas.components.serialization;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vaadin.ui.AbstractComponent;

/**
 * JsonNode que corresponde con la traducción de objetos de tipo
 * {@link AbstractComponent} a JSON interpretables por JsonVaadinConverter.
 * 
 * @since 1.0.0
 */
public class JsonNodeAbstractComponent extends JsonNodeComponent {

	@JsonInclude(Include.NON_NULL)
	private String value;

	public JsonNodeAbstractComponent(Class<?> componentClass) {
		super(componentClass);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "JsonNodeAbstractComponent [value=" + value + ", getId()="
				+ getId() + ", getCaption()=" + getCaption()
				+ ", getIconPath()=" + getIconPath() + ", getStyleName()="
				+ getStyleName() + ", getEnabled()=" + getEnabled()
				+ ", getReadOnly()=" + getReadOnly() + ", getVisible()="
				+ getVisible() + ", getWidth()=" + getWidth() + ", getHeight()="
				+ getHeight() + "]";
	}

}
