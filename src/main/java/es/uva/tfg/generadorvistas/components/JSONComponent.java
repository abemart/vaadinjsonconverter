package es.uva.tfg.generadorvistas.components;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.ui.Component;

/**
 * Interfaz base que implementan todos los componentes de Vaadin Json Converter.
 * {@link AbstractJSONComponent} proporciona una implementación por defecto de
 * todos los métodos de esta interfaz.
 * 
 * @since 1.0.0
 * 
 */
public interface JSONComponent<T extends Component> {

	/**
	 * Indica el id que va a tener el {@link JSONComponent}.
	 * 
	 * @param id
	 *            el id que va a tener el {@link JSONComponent}.
	 */
	public abstract void setId(String id);

	/**
	 * Obtiene el id que tiene el {@link JSONComponent}.
	 * 
	 * @return el id que tiene el {@link JSONComponent}.
	 */
	public abstract String getId();

	/**
	 * Setter para indicar el JSON por el que está formado el componente.
	 * 
	 * @param json
	 *            El JSON por el que está formado el componente.
	 */
	public abstract void setJson(JsonNode json);

	/**
	 * Obtiene el JSON por el que está formado el componente.
	 * 
	 * @return el JSON por el que está formado el componente.
	 */
	public abstract JsonNode getJson();

	/**
	 * Obtiene el componente principal de Vaadin que se ha generado a partir del
	 * JSON.
	 * 
	 * @return el componente principal de Vaadin que se ha generado a partir del
	 *         JSON.
	 */
	public abstract T getVaadinComponent();

	/**
	 * Obtiene el componente de Vaadin que se ha generado a partir del JSON y
	 * que posee el identificador indicado.
	 * 
	 * @param id
	 *            El identificador del componente.
	 * @return el componente de Vaadin que se ha generado a partir del JSON y
	 *         que posee el identificador indicado. Si no existe ningún
	 *         componente con ese identificador devuelve <code>null</code>.
	 */
	public abstract Component getVaadinComponent(String id);

	/**
	 * Obtiene el {@link JSONComponent} que se ha generado a partir del JSON y
	 * que posee el identificador indicado.
	 * 
	 * @param id
	 *            El identificador del componente.
	 * @return el {@link JSONComponent} que se ha generado a partir del JSON y
	 *         que posee el identificador indicado. Si no existe ningún
	 *         componente con ese identificador devuelve <code>null</code>.
	 */
	public abstract JSONComponent<?> getJSONComponent(String id);

	/**
	 * Indica el componente Vaadin que corresponde con el JSON del
	 * {@link JSONComponent}.
	 * 
	 * @param component
	 *            El componente de Vaadin que corresponde con el JSON.
	 */
	public abstract void setVaadinComponent(T component);
}
