package es.uva.tfg.generadorvistas.components.serialization;

import java.util.List;

import com.vaadin.ui.Table;

/**
 * JsonNode que corresponde con la traducción de objetos de tipo {@link Table} a
 * JSON interpretables por JsonVaadinConverter.
 * 
 * @since 1.0.0
 */
public class JsonNodeTable extends JsonNodeComponent {

	private List<JsonNodeColumn> columns;

	public JsonNodeTable() {
		super(Table.class);
	}

	public List<JsonNodeColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<JsonNodeColumn> columns) {
		this.columns = columns;
	}

	@Override
	public String toString() {
		return "JsonNodeTable [columns=" + columns + "]";
	}

}
