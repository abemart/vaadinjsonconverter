package es.uva.tfg.generadorvistas.components.serialization;

import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.vaadin.ui.TabSheet;

/**
 * JsonNode que corresponde con la traducción de objetos de tipo
 * {@link TabSheet} a JSON interpretables por JsonVaadinConverter.
 * 
 * @since 1.0.0
 */
public class JsonNodeTabSheet extends JsonNodeComponent {

	private static final String CONTENT_PROPERTY = "content";
	private static final String TABS_PROPERTY = "tabs";

	private transient @Expose List<JsonNodeTab> tabs;

	public JsonNodeTabSheet() {
		super(TabSheet.class);
	}

	public List<JsonNodeTab> getTabs() {
		return tabs;
	}

	public void setTabs(List<JsonNodeTab> tabs) {
		this.tabs = tabs;
	}

	@Override
	public String toString() {
		return "JsonNodeTabSheet [tabs=" + tabs + "]";
	}

	@Override
	public JsonObject getJsonObject() {
		JsonObject result = new JsonObject();
		JsonObject thisObject = getGson().toJsonTree(this).getAsJsonObject();

		JsonArray tabsArray = new JsonArray();
		for (JsonNodeTab t : tabs) {
			JsonObject tabObject = getGson().toJsonTree(t).getAsJsonObject();

			// Obtenemos el JsonObject del objeto al que contiene la tab
			// para añadirselo correctamente serializado
			JsonObject content = t.getContent().getJsonObject();
			tabObject.add(CONTENT_PROPERTY, content);

			tabsArray.add(tabObject);
		}
		// Añadimos el array de Tabs al objeto principal
		thisObject.add(TABS_PROPERTY, tabsArray);

		// Añadimos el nombre del objeto, 'TabSheet', como root del JSON
		result.add(getComponentClass().getSimpleName(), thisObject);

		return result;
	}

}
