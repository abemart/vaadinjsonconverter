package es.uva.tfg.generadorvistas.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;

/**
 * {@link JSONComponent} diseñado para contener componentes de Vaadin que
 * implementan {@link AbstractOrderedLayout}. Todos los componentes que se
 * traducen a {@link JSONAbstractOrderedLayout} deben extender de
 * AbstractOrderedLayout.
 * 
 * @since 1.0.0
 */
public class JSONAbstractOrderedLayout
		extends AbstractJSONComponent<AbstractOrderedLayout> {

	private List<JSONComponent<?>> listComponents;
	private Map<String, JSONComponent<?>> components;

	public JSONAbstractOrderedLayout() {
		this.listComponents = new ArrayList<>();
		this.components = new HashMap<>();
	}

	/**
	 * Añade un nuevo {@link JSONComponent} a los componentes por los que está
	 * formado el {@link AbstractOrderedLayout}. Unicamente añade el
	 * JSONCompnent si este posee un id.
	 * 
	 * @param jsonComponent
	 *            El JSONComponent que queremos añadir a los componentes por los
	 *            que está formado el {@link AbstractOrderedLayout}.
	 */
	public void addJSONComponent(JSONComponent<?> jsonComponent) {
		// A esta lista siempre se añaden
		listComponents.add(jsonComponent);

		// Añadimos el componente Vaadin al AbstractOrderedLayout
		getVaadinComponent().addComponent(jsonComponent.getVaadinComponent());

		String id = jsonComponent != null ? jsonComponent.getId() : null;
		if (id != null)
			components.put(id, jsonComponent);
	}

	/**
	 * Añade un nuevo {@link JSONComponent} a los componentes por los que está
	 * formado el {@link AbstractOrderedLayout}. Unicamente añade el
	 * JSONCompnent si este posee un id. Además le indica el {@link Alignment}
	 * que posee el componente en el {@link AbstractOrderedLayout} y el expand
	 * ratio.
	 * 
	 * @param jsonComponent
	 *            El JSONComponente que queremos añadir a los componentes por
	 *            los que está formado el {@link AbstractOrderedLayout}.
	 * @param expandRatio
	 *            El expandRatio que se le quiere aplicar al componente.
	 * @param alignment
	 *            El alineado que queremos que posea el componente que vamos a
	 *            añadir.
	 */
	public void addJSONComponent(JSONComponent<?> jsonComponent,
			Alignment alignment, Long expandRatio) {
		if (jsonComponent != null) {
			// Añadimos el componente
			addJSONComponent(jsonComponent);

			Component component = jsonComponent.getVaadinComponent();
			AbstractOrderedLayout orderedLayout = getVaadinComponent();

			if (alignment != null) {
				// Añadimos el alineado al componente
				orderedLayout.setComponentAlignment(component, alignment);
			}

			if (expandRatio != null) {
				orderedLayout.setExpandRatio(component, expandRatio);
			}
		}
	}

	/**
	 * Obtiene el {@link JSONComponent} que coincide con el identificador y que
	 * se encuentra como JSONComponent entre los componentes del
	 * {@link JSONAbstractOrderedLayout}.
	 */
	@Override
	public JSONComponent<?> getJSONComponent(String id) {
		JSONComponent<?> component = null;
		if (id != null) {
			if (getId() != null && getId().equals(id)) {
				component = this;
			} else {
				component = components.get(id);
			}
		}
		return component;
	}

	/**
	 * Obtiene el componente Vaadin que coincide con el identificador y que se
	 * encuentra como componente entre los componentes del
	 * {@link JSONAbstractOrderedLayout}.
	 */
	@Override
	public Component getVaadinComponent(String id) {
		Component vaadinComponent = null;
		JSONComponent<?> jsonComponent = getJSONComponent(id);

		if (jsonComponent != null) {
			if (jsonComponent.getId().equals(id)) {
				vaadinComponent = jsonComponent.getVaadinComponent();
			}
		} else {
			for (JSONComponent<?> c : listComponents) {
				vaadinComponent = c.getVaadinComponent(id);
				if (vaadinComponent != null)
					return vaadinComponent;
			}
		}

		return vaadinComponent;
	}

	@Override
	public String toString() {
		return "JSONAbstractOrderedLayout [components=" + components + ", id="
				+ getId() + ", json=" + getJson() + ", vaadinComponent="
				+ getVaadinComponent() + "]";
	}

}
