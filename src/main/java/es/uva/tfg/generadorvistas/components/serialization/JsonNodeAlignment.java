package es.uva.tfg.generadorvistas.components.serialization;

public class JsonNodeAlignment {

	private String componentId;
	private String alignment;

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getAlignment() {
		return alignment;
	}

	public void setAlignment(String alignment) {
		this.alignment = alignment;
	}

	@Override
	public String toString() {
		return "JsonNodeAlignment [componentId=" + componentId + ", alignment="
				+ alignment + "]";
	}

}
