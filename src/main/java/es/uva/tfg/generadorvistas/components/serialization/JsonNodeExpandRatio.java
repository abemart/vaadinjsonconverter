package es.uva.tfg.generadorvistas.components.serialization;

public class JsonNodeExpandRatio {

	private String componentId;
	private Float expandRatio;

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public Float getExpandRatio() {
		return expandRatio;
	}

	public void setExpandRatio(Float expandRatio) {
		this.expandRatio = expandRatio;
	}

	@Override
	public String toString() {
		return "JsonNodeExpandRatio [componentId=" + componentId
				+ ", expandRatio=" + expandRatio + "]";
	}

}
