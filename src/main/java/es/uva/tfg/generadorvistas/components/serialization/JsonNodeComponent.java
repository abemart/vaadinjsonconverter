package es.uva.tfg.generadorvistas.components.serialization;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * JsonNode base que todos los componentes de JsonVaadinComponent extienden.
 * 
 * @since 1.0.0
 */
public class JsonNodeComponent extends AbstractJsonNodeComponent {

	private String id;
	private String caption;
	private String iconPath;
	private String styleName;
	private Boolean enabled;
	private Boolean readOnly;
	private Boolean visible;
	private JsonNodeWidth width;
	private JsonNodeHeight height;

	public JsonNodeComponent(Class<?> componentClass) {
		super(componentClass);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getIconPath() {
		return iconPath;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getStyleName() {
		return styleName;
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public JsonNodeWidth getWidth() {
		return width;
	}

	public void setWidth(JsonNodeWidth width) {
		this.width = width;
	}

	public JsonNodeHeight getHeight() {
		return height;
	}

	public void setHeight(JsonNodeHeight height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "JsonNodeComponent [id=" + id + ", caption=" + caption
				+ ", iconPath=" + iconPath + ", styleName=" + styleName
				+ ", enabled=" + enabled + ", readOnly=" + readOnly
				+ ", visible=" + visible + ", width=" + width + ", height="
				+ height + "]";
	}

	@Override
	public JsonObject getJsonObject() {
		JsonObject result = new JsonObject();
		JsonElement element = getGson().toJsonTree(this);
		result.add(getComponentClass().getSimpleName(), element);

		return result;
	}

}
