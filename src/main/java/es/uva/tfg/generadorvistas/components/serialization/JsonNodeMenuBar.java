package es.uva.tfg.generadorvistas.components.serialization;

import java.util.List;

import com.google.gwt.user.client.ui.MenuBar;

/**
 * JsonNode que corresponde con la traducción de objetos de tipo {@link MenuBar}
 * a JSON interpretables por JsonVaadinConverter.
 * 
 * @since 1.0.0
 */
public class JsonNodeMenuBar extends JsonNodeComponent {

	private List<JsonNodeMenuItem> menuItems;

	public JsonNodeMenuBar() {
		super(MenuBar.class);
	}

	public List<JsonNodeMenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(List<JsonNodeMenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	@Override
	public String toString() {
		return "JsonNodeMenuBar [menuItems=" + menuItems + "]";
	}

}
