package es.uva.tfg.generadorvistas.components.serialization;

/**
 * JsonNode que corresponde con la interpretación de la propiedad Width de los
 * componentes de Vaadin, en los JSON interpretables por JsonVaadinConverter.
 * 
 * @since 1.0.0
 */
public class JsonNodeWidth {

	private Float width;
	private String unit;

	public Float getWidth() {
		return width;
	}

	public void setWidth(Float width) {
		this.width = width;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		return "JsonNodeWidth [width=" + width + ", unit=" + unit + "]";
	}

}
