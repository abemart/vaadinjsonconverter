package es.uva.tfg.generadorvistas.components.serialization;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

public abstract class AbstractJsonNodeComponent {

	private transient @Expose Class<?> componentClass;
	private transient @Expose Gson gson;

	public AbstractJsonNodeComponent(Class<?> componentClass) {
		this.gson = new Gson();
		this.componentClass = componentClass;
	}

	public Gson getGson() {
		return gson;
	}

	public Class<?> getComponentClass() {
		return componentClass;
	}

	public abstract JsonObject getJsonObject();
}
