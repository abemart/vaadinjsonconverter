package es.uva.tfg.generadorvistas.components;

import com.vaadin.ui.AbstractComponent;

/**
 * {@link JSONComponent} diseñado para contener componentes de Vaadin que
 * implementan {@link AbstractComponent}. Todos los componentes que se traducen a
 * {@link JSONAbstractComponent} deben extender de AbstractField.
 */
public class JSONAbstractComponent
		extends AbstractJSONComponent<AbstractComponent> {

}
