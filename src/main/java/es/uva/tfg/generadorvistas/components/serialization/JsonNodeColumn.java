package es.uva.tfg.generadorvistas.components.serialization;

public class JsonNodeColumn {

	private String propertyId;
	private String header;
	private String columnType;

	public JsonNodeColumn() {
		this.columnType = "String";
	}

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getColumnType() {
		return columnType;
	}

	@Override
	public String toString() {
		return "JsonNodeColumn [propertyId=" + propertyId + ", header=" + header
				+ ", columnType=" + columnType + "]";
	}

}
