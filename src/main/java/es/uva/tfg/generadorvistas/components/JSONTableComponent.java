package es.uva.tfg.generadorvistas.components;

import com.vaadin.ui.Table;

/**
 * {@link JSONComponent} diseñado para contener componentes de Vaadin de tipo
 * {@link Table}. Todos los componentes que se traducen a
 * {@link JSONTableComponent} deben ser de tipo {@link Table}.
 * 
 * @since 1.0.0
 */
public class JSONTableComponent extends AbstractJSONComponent<Table> {

}
