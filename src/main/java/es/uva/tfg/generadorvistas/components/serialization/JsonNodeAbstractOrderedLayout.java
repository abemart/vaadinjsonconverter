package es.uva.tfg.generadorvistas.components.serialization;

import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.vaadin.ui.AbstractOrderedLayout;

/**
 * JsonNode que corresponde con la traducción de objetos de tipo
 * {@link AbstractOrderedLayout} a JSON interpretables por JsonVaadinConverter.
 * 
 * @since 1.0.0
 */
public class JsonNodeAbstractOrderedLayout extends JsonNodeComponent {

	private static final String COMPONENTS_PROPERTY = "components";

	private Boolean spacing;
	private Boolean margin;
	private transient @Expose List<JsonNodeComponent> components;
	private List<JsonNodeAlignment> alignments;
	private List<JsonNodeExpandRatio> expandRatio;

	public JsonNodeAbstractOrderedLayout(Class<?> componentClass) {
		super(componentClass);
	}

	public Boolean getSpacing() {
		return spacing;
	}

	public void setSpacing(Boolean spacing) {
		this.spacing = spacing;
	}

	public Boolean getMargin() {
		return margin;
	}

	public void setMargin(Boolean margin) {
		this.margin = margin;
	}

	public List<JsonNodeComponent> getComponents() {
		return components;
	}

	public void setComponents(List<JsonNodeComponent> components) {
		this.components = components;
	}

	public List<JsonNodeAlignment> getAlignments() {
		return alignments;
	}

	public void setAlignments(List<JsonNodeAlignment> alignments) {
		this.alignments = alignments;
	}

	public List<JsonNodeExpandRatio> getExpandRatio() {
		return expandRatio;
	}

	public void setExpandRatio(List<JsonNodeExpandRatio> expandRatio) {
		this.expandRatio = expandRatio;
	}

	@Override
	public String toString() {
		return "JsonNodeAbstractOrderedLayout [spacing=" + spacing + ", margin="
				+ margin + ", components=" + components + ", alignments="
				+ alignments + ", expandRatio=" + expandRatio + "]";
	}

	@Override
	public JsonObject getJsonObject() {
		JsonObject result = new JsonObject();
		JsonObject thisObject = getGson().toJsonTree(this).getAsJsonObject();

		thisObject.remove(COMPONENTS_PROPERTY);

		JsonArray componentsArray = new JsonArray();
		for (JsonNodeComponent c : components) {
			JsonObject element = c.getJsonObject();
			componentsArray.add(element);
		}

		thisObject.add(COMPONENTS_PROPERTY, componentsArray);

		result.add(getComponentClass().getSimpleName(), thisObject);

		return result;
	}

}
