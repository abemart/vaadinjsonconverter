package es.uva.tfg.generadorvistas.components;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaadin.ui.Component;

/**
 * Clase abstracta que define una implementación por defecto de la interfaz
 * {@link JSONComponent}.
 * 
 * @since 1.0.0
 */
public abstract class AbstractJSONComponent<T extends Component>
		implements JSONComponent<T> {

	private String id;
	private T component;
	private JsonNode json;

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setJson(JsonNode json) {
		this.json = json;
	}

	@Override
	public JsonNode getJson() {
		return json;
	}

	@Override
	public T getVaadinComponent() {
		return component;
	}
	
	@Override
	public Component getVaadinComponent(String id) {
		return this.id != null && this.id.equals(id) ? component : null;
	}

	@Override
	public JSONComponent<?> getJSONComponent(String id) {
		return this.id != null && this.id.equals(id) ? this : null;
	}

	@Override
	public void setVaadinComponent(T component) {
		this.component = component;
	}

	@Override
	public String toString() {
		return "AbstractJSONComponent [id=" + id + ", component=" + component
				+ ", json=" + json + "]";
	}
	
}
