package es.uva.tfg.generadorvistas.components.serialization;

/**
 * JsonNode que corresponde con la interpretación de la propiedad Height de los
 * componentes de Vaadin, en los JSON interpretables por JsonVaadinConverter.
 * 
 * @since 1.0.0
 */
public class JsonNodeHeight {

	private Float height;
	private String unit;

	public Float getHeight() {
		return height;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		return "JsonNodeHeight [height=" + height + ", unit=" + unit + "]";
	}

}
