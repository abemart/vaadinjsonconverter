package es.uva.tfg.generadorvistas.components.serialization;

import com.google.gson.annotations.Expose;

public class JsonNodeTab {

	private String caption;
	private String icon;
	private transient @Expose JsonNodeComponent content;

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public JsonNodeComponent getContent() {
		return content;
	}

	public void setContent(JsonNodeComponent content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "JsonNodeTab [caption=" + caption + ", icon=" + icon
				+ ", content=" + content + "]";
	}

}
